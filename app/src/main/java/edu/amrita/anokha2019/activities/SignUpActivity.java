package edu.amrita.anokha2019.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.helpers.CollegeList;
import edu.amrita.anokha2019.helpers.SSLPin;
import edu.amrita.anokha2019.helpers.checkNetwork;
import okhttp3.CertificatePinner;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SignUpActivity extends AppCompatActivity {

    TextView terms;
    RadioGroup radioGroup;
    EditText name,emailid,phonenum,Regpassword,RegConfirmpass;
    AutoCompleteTextView college;
    String colleges[];
    Button signup;

    Response response;
    CertificatePinner certificatePinner;

    String url_register,res;

    ProgressDialog dialog;
    String sName,sEmail,sPhone,sRegpass,sRegConfirmpass,sCollege,sGender;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Toolbar toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Create an account");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        terms=findViewById(R.id.terms);
        college=findViewById(R.id.collegename);
        name=findViewById(R.id.name);
        emailid=findViewById(R.id.emailid);
        phonenum=findViewById(R.id.phonenum);
        Regpassword=findViewById(R.id.Regpassword);
        RegConfirmpass=findViewById(R.id.RegConfirmpass);
        radioGroup=findViewById(R.id.radioGroup);
        signup=findViewById(R.id.signup);
        url_register=getResources().getString(R.string.url_register);
        
        radioGroup.clearCheck();     
        
        

        
        CollegeList obj = new CollegeList();
        colleges = obj.collegelist;
        ArrayAdapter<String> clgAdapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.spinneritem,colleges);
        college.setThreshold(3);
        college.setAdapter(clgAdapter);
        college.setLines(1);
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignUpActivity.this,TermsAndConditions.class));
            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkNetwork.isConnected(SignUpActivity.this) &&checkifedittextEmpty() && Confirmpasswordvalidation() && emailValidation() && phonenumbervalidation())
                {
                    Log.e("DATA",name.getText().toString()+phonenum.getText().toString()+Regpassword.getText().toString()+college.getText().toString()+emailid.getText().toString());
                    new okSignup().execute();
                }
            }
        });
    }




    private class okSignup extends AsyncTask<Void,Void,Void>{
        @Override
        protected void onPreExecute() {

            sName=name.getText().toString();
            sPhone=phonenum.getText().toString();
            sRegpass=Regpassword.getText().toString();
            sCollege=college.getText().toString();
            sEmail=emailid.getText().toString();
            RadioButton r = (RadioButton) findViewById(radioGroup.getCheckedRadioButtonId());
            sGender = r.getText().toString();



            dialog=new ProgressDialog(SignUpActivity.this);
            dialog.setMessage("Creating account ..");
            dialog.setCancelable(false);
            dialog.show();
            super.onPreExecute();
        }


        @Override
        protected Void doInBackground(Void... voids) {
            try{
                certificatePinner=SSLPin.getCertPinner(SignUpActivity.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();

                RequestBody body = new FormBody.Builder()
                        .add("email",sEmail)
                        .add("password",sRegpass)
                        .add("gender",sGender)
                        .add("phone_number",sPhone)
                        .add("college",sCollege)
                        .add("name",sName)
                        .add("registered_with","android")
                        .build();

                Request request = new Request.Builder()
                        .url(url_register)
                        .post(body)
                        .build();
                response=client.newCall(request).execute();
                res=response.body().string();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            try{
                JSONObject jsonObject=new JSONObject(res);
                if(!jsonObject.getBoolean("success")){
                    Toast.makeText(SignUpActivity.this,jsonObject.getString("error"),Toast.LENGTH_LONG).show();
                }else if(jsonObject.getBoolean("success")){
                    final android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(SignUpActivity.this);
                    alertDialog.setMessage("Your Anokha account was created successfully. Please check your mail and verify the same.");
                    alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            finish();
                        }
                    });
                    android.support.v7.app.AlertDialog dialog = alertDialog.create();
                    dialog.setCancelable(false);
                    dialog.show();
                }else{
                    Toast.makeText(SignUpActivity.this,"Error connecting to server",Toast.LENGTH_SHORT).show();
                }
            }catch (Exception e)
            {
                Toast.makeText(SignUpActivity.this,"Error connecting to server",Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            if(dialog.isShowing())
                dialog.dismiss();
            super.onPostExecute(aVoid);
        }
    }




    public boolean checkifedittextEmpty(){

        if (TextUtils.isEmpty(name.getText().toString())) {
            name.setError("This field cannot be empty");
            return false;
        }

        else if (TextUtils.isEmpty(emailid.getText().toString())){
            emailid.setError("This field cannot be empty");
            return false;

        }
        else if (TextUtils.isEmpty(phonenum.getText().toString())){
            phonenum.setError("This field cannot be empty");
            return false;

        }
        else if (TextUtils.isEmpty(Regpassword.getText().toString())){
            Regpassword.setError("This field cannot be empty");
            return false;

        }
        else if (TextUtils.isEmpty(RegConfirmpass.getText().toString())){
            RegConfirmpass.setError("This field cannot be empty");
            return false;

        }

        else if (TextUtils.isEmpty(college.getText().toString())){
            college.setError("This field cannot be empty");
            return false;
        }


        else if(radioGroup.getCheckedRadioButtonId() == -1){
            Toast.makeText(getApplicationContext(),"Please select your gender",Toast.LENGTH_SHORT).show();
            return false;

        }
        else {

            return true;
        }

    }


    public boolean Confirmpasswordvalidation(){
        sRegpass = Regpassword.getText().toString();
        sRegConfirmpass = RegConfirmpass.getText().toString();
        if (sRegpass.equals(sRegConfirmpass)){
            return  true;
        }
        else{
            RegConfirmpass.setError("Passwords don't match");
            return  false;
        }

    }


    public boolean emailValidation(){
        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern p = Pattern.compile(EMAIL_STRING);
        Matcher check = p.matcher(emailid.getText().toString());

        Boolean b = check.matches();
        if (!b){
            emailid.setError("Enter valid email address");
            return false;
        }else{

            return  true;
        }



    }


    public boolean phonenumbervalidation(){
        sPhone = phonenum.getText().toString();
        boolean b = android.util.Patterns.PHONE.matcher(sPhone).matches();
        if (b && sPhone.length() == 10 ){
            return true;
        }else {
            phonenum.setError("Enter valid mobile number");
            return false;
        }



    }


}

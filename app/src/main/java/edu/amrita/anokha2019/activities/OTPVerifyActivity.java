package edu.amrita.anokha2019.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.iwgang.countdownview.CountdownView;
import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.helpers.GlobalData;
import edu.amrita.anokha2019.helpers.PinEntryEditText;
import edu.amrita.anokha2019.helpers.SSLPin;
import edu.amrita.anokha2019.helpers.Utils;
import okhttp3.CertificatePinner;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import swarajsaaj.smscodereader.interfaces.OTPListener;
import swarajsaaj.smscodereader.receivers.OtpReader;

public class OTPVerifyActivity extends AppCompatActivity implements OTPListener {

    PinEntryEditText otp;
    TextView helptext;
    Button verify,resend;
    ProgressDialog dialog;
    CountdownView mCvCountdownView;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpverify);
        android.support.v7.widget.Toolbar toolbar=findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        otp=findViewById(R.id.otp);
        helptext=findViewById(R.id.help_text);
        verify=findViewById(R.id.verify);
        resend=findViewById(R.id.resend_otp);
        mCvCountdownView = (CountdownView)findViewById(R.id.timer);

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!otp.getText().toString().isEmpty())
                new verify().execute(otp.getText().toString());
                else
                    Toast.makeText(OTPVerifyActivity.this, "Please enter the OTP", Toast.LENGTH_SHORT).show();
            }
        });
        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new resetPassword().execute(getIntent().getStringExtra("email"));
            }
        });

        turnOnTimer();

        dialog=new ProgressDialog(this);
        int sms_permission =ActivityCompat.checkSelfPermission(this,Manifest.permission.RECEIVE_SMS);
        if(sms_permission == PackageManager.PERMISSION_GRANTED ) {
            helptext.setText("Please wait while we get things done for you.\nWaiting for OTP");
        }
        OtpReader.bind(this,"ANOKHA");
    }

    private void turnOnTimer(){
        resend.setEnabled(false);
        mCvCountdownView.start(180000); // Millisecond
        new CountDownTimer(180000, 10) { //Set Timer for 3 minutes
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                resend.setEnabled(true);
            }
        }.start();
    }

    @SuppressLint("StaticFieldLeak")
    private class verify extends AsyncTask<String,Void,Void>{

        private String res;

        @Override
        protected void onPreExecute() {
            try {
                dialog.setMessage("Verifying OTP");
                dialog.setCancelable(false);
                dialog.show();
            }catch (Exception e){
                e.printStackTrace();
                Crashlytics.log(e.getMessage());
            }
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            try{
                CertificatePinner certificatePinner = SSLPin.getCertPinner(OTPVerifyActivity.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();

                Request request;
                RequestBody body;

                body=new FormBody.Builder()
                        .add("otp",strings[0])
                        .build();

                request=new Request.Builder()
                        .url("https://anokha.amrita.edu/api/recover/otp")
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                assert response.body() != null;
                res=response.body().string();
                Log.e("RESPONSE",res);

            }catch (Exception e){
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(dialog!=null && dialog.isShowing()){
                dialog.dismiss();
            }
            try {
                JSONObject response = new JSONObject(res);
                if(response.getBoolean("success")){
                    GlobalData.token=response.getJSONObject("data").getString("token");
                    startActivity(new Intent(OTPVerifyActivity.this,ChangePasswordActivity.class).putExtra("current_disable",true));
                    finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            super.onPostExecute(aVoid);
        }
    }

    @Override
    public void otpReceived(String messageText) {
        Pattern pattern = Pattern.compile("(\\d{4})");
        if(messageText!=null){
            Matcher m = pattern.matcher(messageText);
            if(m.find()) {
                otp.setText(m.group(0));
                Log.e("Otp",m.group(0));
                new verify().execute(m.group(0));
            }
            else
            {
                Log.e("ERROR","Error occurred while parsing OTP");
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    class resetPassword extends AsyncTask<String,Void,Void>{
        private String res;

        @Override
        protected void onPreExecute() {
            try {
                dialog = new ProgressDialog(OTPVerifyActivity.this);
                dialog.setMessage("Please wait");
                dialog.setCancelable(false);
                dialog.show();
            }catch (Exception e){
                e.printStackTrace();
                Crashlytics.log(e.getMessage());
            }
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            try{
                CertificatePinner certificatePinner = SSLPin.getCertPinner(OTPVerifyActivity.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();

                RequestBody body = new FormBody.Builder()
                        .add("email",strings[0])
                        .build();

                Request request = new Request.Builder()
                        .url("https://anokha.amrita.edu/api/recover")
                        .addHeader("device","android")
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                assert response.body() != null;
                res=response.body().string();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if(dialog.isShowing())
                dialog.dismiss();
            try {
                JSONObject response = new JSONObject(res);
                if(response.getBoolean("success")) {
                    turnOnTimer();
                }
                else{
                    Dialog dew = new Dialog(OTPVerifyActivity.this);
                    dew.setContentView(R.layout.dialog_regsuccessful);
                    TextView t = (TextView)dew.findViewById(R.id.textmsg);
                    t.setText(response.getString("error"));
                    dew.show();
                    dew.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            finish();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            super.onPostExecute(aVoid);
        }
    }
}

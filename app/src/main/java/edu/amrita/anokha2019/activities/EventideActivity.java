package edu.amrita.anokha2019.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.helpers.GlobalData;
import edu.amrita.anokha2019.adapters.EventideAdapter;
import edu.amrita.anokha2019.helpers.SSLPin;
import edu.amrita.anokha2019.models.Artist;
import okhttp3.CertificatePinner;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static edu.amrita.anokha2019.helpers.GlobalData.mediaPlayer;

public class EventideActivity extends AppCompatActivity {

    EventideAdapter adapter;
    DotsIndicator dotsIndicator;
    ViewPager viewPager;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventide);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Eventide");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        viewPager= (ViewPager) findViewById(R.id.viewpager);
        dotsIndicator = (DotsIndicator) findViewById(R.id.dots_indicator);
        progressBar=findViewById(R.id.spin_kit);
        new getArtistsData().execute();
    }
    @SuppressLint("StaticFieldLeak")
    class getArtistsData extends AsyncTask<Void,Void,Void> {
        private CertificatePinner certificatePinner;

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                certificatePinner=SSLPin.getCertPinner(EventideActivity.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();

                Request request = new Request.Builder()
                        .url("https://anokha.amrita.edu/api/eventide/artists")
                        .get()
                        .build();
                Response response = client.newCall(request).execute();
                assert response.body() != null;
                GlobalData.artists=new JSONObject(response.body().string());
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            GlobalData.artistsList=new ArrayList<>();
            try{
                try {
                    JSONArray jsonArray = GlobalData.artists.getJSONArray("data");
                    if (GlobalData.artists.getBoolean("status") && jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); ++i) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            Artist artist=new Artist(jsonObject.getString("name"),jsonObject.getString("description"),jsonObject.getString("image"),getMusic(jsonObject.getString("name")));
                            GlobalData.artistsList.add(artist);
                        }
                    }else{
                        Toast.makeText(EventideActivity.this, "Coming soon", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(EventideActivity.this, "Unexpected error", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(EventideActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
                }

            }catch (Exception e){
                Toast.makeText(EventideActivity.this,"Some error occured.",Toast.LENGTH_SHORT).show();
                finish();
                e.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);
            adapter = new EventideAdapter(EventideActivity.this,getSupportFragmentManager());
            viewPager.setAdapter(adapter);
            dotsIndicator.setViewPager(viewPager);

            super.onPostExecute(aVoid);
        }
    }

    private int getMusic(String name){
        if(name.toLowerCase().contains("naresh"))
            return R.raw.naresh;
        else
            return R.raw.neha;
    }
    @Override
    protected void onPause() {
        if(mediaPlayer!=null)mediaPlayer.stop();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if(mediaPlayer!=null)mediaPlayer.stop();
        super.onBackPressed();
    }
}
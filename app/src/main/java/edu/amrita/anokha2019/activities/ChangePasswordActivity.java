package edu.amrita.anokha2019.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Objects;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.helpers.GlobalData;
import edu.amrita.anokha2019.helpers.SSLPin;
import edu.amrita.anokha2019.helpers.Utils;
import edu.amrita.anokha2019.helpers.checkNetwork;
import okhttp3.CertificatePinner;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ChangePasswordActivity extends AppCompatActivity {

    EditText oldp,newp,rtnewp;
    Response response;
    CertificatePinner certificatePinner;
    ProgressDialog dialog;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        oldp=findViewById(R.id.current);
        newp=findViewById(R.id.new_password);
        rtnewp=findViewById(R.id.retype_new_password);
        dialog=new ProgressDialog(this);

        if(getIntent().getBooleanExtra("current_disable",false)){
            oldp.setText("N/A");
            oldp.setVisibility(View.GONE);
        }

        if(getIntent().getBooleanExtra("password_reset",false)){
            Dialog dew = new Dialog(ChangePasswordActivity.this);
            dew.setContentView(R.layout.dialog_regsuccessful);
            TextView t = (TextView)dew.findViewById(R.id.textmsg);
            t.setText("Your password was reset. Please set a new one for security reasons.");
            dew.show();
        }

        Button submit=findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                if(oldp.getText().toString().isEmpty() || newp.getText().toString().isEmpty() || rtnewp.getText().toString().isEmpty()){
                    Toast.makeText(ChangePasswordActivity.this,"Please fill up all the fields",Toast.LENGTH_LONG).show();
                }else{
                    if(newp.getText().toString().equals(rtnewp.getText().toString())) {
                        if (checkNetwork.isConnected(ChangePasswordActivity.this))
                            new update().execute();
                    }
                    else{
                            Toast.makeText(ChangePasswordActivity.this,"Passwords don't match",Toast.LENGTH_LONG).show();
                        }
                }
            }
        });
    }

    public void hideKeyboard(){
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        if(inputManager.isAcceptingText())
            inputManager.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @SuppressLint("StaticFieldLeak")
    class update extends AsyncTask<Void,Void,Void>{
        String res;
        @Override
        protected void onPreExecute() {
            dialog.setMessage("Resetting password");
            dialog.setCancelable(false);
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{

                certificatePinner=SSLPin.getCertPinner(ChangePasswordActivity.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();

                Request request;

                if(Utils.isTokenExpired() && !getIntent().getBooleanExtra("current_disable",false)) {
                    RequestBody body = new FormBody.Builder()
                            .add("email", GlobalData.username)
                            .add("password", GlobalData.password)
                            .build();

                    request = new Request.Builder()
                            .url(getString(R.string.url_login))
                            .post(body)
                            .build();
                    response = client.newCall(request).execute();
                    res = response.body().string();

                    try {
                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getBoolean("success")) {
                            JSONObject data = jsonObject.getJSONObject("data");
                            GlobalData.token = data.getString("token");
                        }
                    } catch (Exception e) {
                        //Toast.makeText(RegisteredEvents.this,"Error connecting to server",Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }

                RequestBody body = new FormBody.Builder()
                        .add("password",newp.getText().toString().trim())
                        .build();

                request = new Request.Builder()
                        .addHeader("Authorization","Bearer "+GlobalData.token)
                        .url("https://anokha.amrita.edu/api/user/updatePassword")
                        .post(body)
                        .build();
                response=client.newCall(request).execute();
                res=response.body().string();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            try{

                JSONObject result=new JSONObject(res);
                Log.e("RESULT",result.toString());
                if(result.getBoolean("success")) {
                    Toast.makeText(ChangePasswordActivity.this, result.getString("data"), Toast.LENGTH_LONG).show();
                    finish();
                }
                else
                    Toast.makeText(ChangePasswordActivity.this,result.getString("error"),Toast.LENGTH_LONG).show();

            }catch (Exception e){
                e.printStackTrace();
            }
            if(dialog.isShowing())
                dialog.dismiss();
            super.onPostExecute(aVoid);
        }
    }


}

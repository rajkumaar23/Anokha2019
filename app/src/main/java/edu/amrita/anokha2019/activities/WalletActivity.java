package edu.amrita.anokha2019.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.adapters.WalletAdapter;
import edu.amrita.anokha2019.helpers.GlobalData;
import edu.amrita.anokha2019.helpers.SSLPin;
import edu.amrita.anokha2019.helpers.Utils;
import edu.amrita.anokha2019.models.Event;
import edu.amrita.anokha2019.models.WalletTransaction;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class WalletActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    ArrayList<WalletTransaction> transactions;
    ArrayList<String> transactionTitles;
    TextView balanceView;
    ListView transactionsView;
    String faq,intro,cashback;
    Button feedback,FAQ,cashBackButton;
    private Toolbar toolbar;
    private String feedbackUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        transactions=new ArrayList<>();
        balanceView=findViewById(R.id.balance);
        transactionsView=findViewById(R.id.transactions);
        feedback=findViewById(R.id.feedback);
        FAQ=findViewById(R.id.faq);
        cashBackButton=findViewById(R.id.cashback);
        toolbar=findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(feedbackUrl!=null && !feedbackUrl.trim().isEmpty()) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(feedbackUrl));
                    try{
                        startActivity(i);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
        cashBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cashback!=null && !cashback.trim().isEmpty())
                    startActivity(new Intent(WalletActivity.this,WalletCashBackActivity.class).putExtra("cashback",cashback));
            }
        });
        FAQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(faq!=null && !faq.trim().isEmpty())
                startActivity(new Intent(WalletActivity.this,WalletFAQActivity.class).putExtra("faq",faq));
            }
        });
        transactionTitles=new ArrayList<>();
        progressDialog=new ProgressDialog(this);
        new getData().execute();
    }

    @SuppressLint("StaticFieldLeak")
    class getData extends AsyncTask<Void,Void,Void>{
        private String res;
        private JSONObject transactionsJSON;
        private Response response;

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(SSLPin.getCertPinner(WalletActivity.this))
                        .build();

                Request request;

                //Refreshing token
                if(Utils.isTokenExpired()) {
                    RequestBody body = new FormBody.Builder()
                            .add("email", GlobalData.username)
                            .add("password", GlobalData.password)
                            .build();

                    request = new Request.Builder()
                            .url(getString(R.string.url_login))
                            .post(body)
                            .build();
                    response = client.newCall(request).execute();
                    assert response.body() != null;
                    res = response.body().string();

                    try {
                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getBoolean("success")) {
                            JSONObject data = jsonObject.getJSONObject("data");
                            GlobalData.token = data.getString("token");
                        }
                    } catch (Exception e) {
                        //Toast.makeText(RegisteredEvents.this,"Error connecting to server",Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }

                request = new Request.Builder()
                        .url(getString(R.string.domain)+"/api/wallet/details")
                        .addHeader("Authorization","Bearer "+GlobalData.token)
                        .get()
                        .build();
                response=client.newCall(request).execute();
                assert response.body() != null;
                transactionsJSON=new JSONObject(response.body().string());
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void onPostExecute(Void aVoid) {
            try{
                faq=transactionsJSON.getJSONObject("content").getString("faq");
                cashback=transactionsJSON.getJSONObject("content").getString("cashback");
                intro=transactionsJSON.getJSONObject("content").getString("intro");
                feedbackUrl=transactionsJSON.getJSONObject("content").getString("feedback");
            }catch (Exception e){
                finish();
                Toast.makeText(WalletActivity.this, "An unexpected error occurred", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            if(response.isSuccessful()) {
                try {
                    balanceView.setText(getString(R.string.Rs) + " " + transactionsJSON.getString("balance"));
                    toolbar.setSubtitle("Card No : "+transactionsJSON.getString("card_no"));
                    for (int i = 0; i < transactionsJSON.getJSONArray("data").length(); ++i) {
                        JSONObject item = transactionsJSON.getJSONArray("data").getJSONObject(i);
                        String tid, details, vendor_name, amount, transacted_at;
                        tid = item.getString("tid");
                        details = item.getString("details");
                        vendor_name = item.getString("vendor_name");
                        amount = item.getString("amount");
                        transacted_at = item.getString("transacted_at");
                        transactions.add(new WalletTransaction(tid, details, vendor_name, amount, transacted_at));
                        transactionTitles.add(details);
                    }
                    WalletAdapter adapter = new WalletAdapter(WalletActivity.this, transactions);
                    //ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(WalletActivity.this,android.R.layout.simple_list_item_1,transactionTitles);
                    transactionsView.setAdapter(adapter);
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else if(response.code() == 404){
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                setIntroMode();
            }else{
                Toast.makeText(WalletActivity.this, "An unexpected error occurred", Toast.LENGTH_SHORT).show();
                finish();
            }
            super.onPostExecute(aVoid);
        }
    }


    private void setIntroMode(){
        transactionsView.setVisibility(View.GONE);
        findViewById(R.id.balance_container).setVisibility(View.GONE);
        findViewById(R.id.titletransactions).setVisibility(View.GONE);
        findViewById(R.id.intro_container).setVisibility(View.VISIBLE);
        ((TextView)findViewById(R.id.content)).setText(intro);
    }
}

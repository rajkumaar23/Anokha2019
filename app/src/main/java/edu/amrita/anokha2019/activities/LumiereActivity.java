package edu.amrita.anokha2019.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.helpers.GlobalData;
import edu.amrita.anokha2019.adapters.LumiereAdapter;
import edu.amrita.anokha2019.helpers.SSLPin;
import edu.amrita.anokha2019.helpers.Utils;
import edu.amrita.anokha2019.models.Speaker;
import okhttp3.CertificatePinner;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LumiereActivity extends AppCompatActivity {

    LumiereAdapter adapter;
    DotsIndicator dotsIndicator;
    ViewPager viewPager;
    CertificatePinner certificatePinner;
    ProgressBar progressBar;
    ProgressDialog dialog;
    Response response;
    String url_speakers;
    Button pay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lumiere);
        android.support.v7.widget.Toolbar toolbar=findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        viewPager= (ViewPager) findViewById(R.id.viewpager);
        pay=findViewById(R.id.pay);
        pay.setEnabled(false);
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new pay().execute();
            }
        });
        dialog=new ProgressDialog(this);
        progressBar=findViewById(R.id.spin_kit);
        url_speakers=getResources().getString(R.string.url_speakers);

        new getSpeakerData().execute();
    }

    @SuppressLint("StaticFieldLeak")
    class getSpeakerData extends AsyncTask<Void,Void,Void>{
        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                certificatePinner=SSLPin.getCertPinner(LumiereActivity.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();

                Request request = new Request.Builder()
                        .url(url_speakers)
                        .get()
                        .build();
                response=client.newCall(request).execute();
                GlobalData.speakers=new JSONObject(response.body().string());
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            GlobalData.speakersList=new ArrayList<>();
            try{
                try {
                    if(response.isSuccessful()) {
                        JSONArray jsonArray = GlobalData.speakers.getJSONArray("data");
                        if (GlobalData.speakers.getBoolean("success") && jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); ++i) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                Speaker speaker = new Speaker(jsonObject.getString("speaker"), jsonObject.getString("designation"), jsonObject.getString("description"), jsonObject.getString("img_url"));
                                GlobalData.speakersList.add(speaker);
                            }
                            pay.setEnabled(true);
                        } else {
                            Toast.makeText(LumiereActivity.this, "Coming soon", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }else{
                        Toast.makeText(LumiereActivity.this, "Coming soon", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(LumiereActivity.this, "Unexpected error", Toast.LENGTH_SHORT).show();
                    finish();
                }
                
            }catch (Exception e){
                Toast.makeText(LumiereActivity.this,"An unexpected error occured.",Toast.LENGTH_SHORT).show();
                finish();
                e.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);
            adapter = new LumiereAdapter(LumiereActivity.this,getSupportFragmentManager());

            viewPager.setAdapter(adapter);
            dotsIndicator = (DotsIndicator) findViewById(R.id.dots_indicator);
            dotsIndicator.setViewPager(viewPager);

            super.onPostExecute(aVoid);
        }
    }

    @SuppressLint("StaticFieldLeak")
    class pay extends AsyncTask<Void,Void,Void>{

        private String res;

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Please wait");
            dialog.setCancelable(false);
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                certificatePinner = SSLPin.getCertPinner(LumiereActivity.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();

                RequestBody body;
                Request request;
                if (Utils.isTokenExpired()) {

                    body = new FormBody.Builder()
                            .add("email", GlobalData.username)
                            .add("password", GlobalData.password)
                            .build();

                    request = new Request.Builder()
                            .url("https://anokha.amrita.edu/api/login")
                            .post(body)
                            .build();
                    response = client.newCall(request).execute();
                    assert response.body() != null;
                    res = response.body().string();
                    GlobalData.token = new JSONObject(res).getJSONObject("data").getString("token");
                }


                request = new Request.Builder()
                        .url("https://anokha.amrita.edu/api/lumiere/register/online")
                        .addHeader("Authorization", "Bearer " + GlobalData.token)
                        .get()
                        .build();
                response = client.newCall(request).execute();
                assert response.body() != null;
                res = response.body().string();

            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(dialog!=null && dialog.isShowing()){
                dialog.dismiss();
            }
            try {
                JSONObject response  = new JSONObject(res);
                if(response.getBoolean("success")){
                    startActivity(new Intent(LumiereActivity.this,PaymentWebView.class).putExtra("webview",response.getString("message")));
                }else if(response.has("error")){
                    Dialog dew = new Dialog(LumiereActivity.this);
                    dew.setContentView(R.layout.dialog_regsuccessful);
                    TextView t = (TextView)dew.findViewById(R.id.textmsg);
                    t.setText(response.getString("error"));
                    dew.show();
                }
                else{
                    Toast.makeText(LumiereActivity.this, "An unexpected error occurred.", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(LumiereActivity.this, "An unexpected error occurred.", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            super.onPostExecute(aVoid);
        }
    }
}

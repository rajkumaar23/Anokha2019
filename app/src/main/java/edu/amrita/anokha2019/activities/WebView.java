package edu.amrita.anokha2019.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebViewClient;

import edu.amrita.anokha2019.R;

public class WebView extends AppCompatActivity {

    ProgressDialog dialog ;
    @Override
    protected void onDestroy() {
        dismissProgressDialog();
        super.onDestroy();
    }
    public void dismissProgressDialog(){
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }
    public void showProgressDialog(){

        dialog.setMessage("Loading..");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        android.support.v7.widget.Toolbar toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Anokha");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Bundle b = getIntent().getExtras();
        assert b != null;
        String webviewlink = b.getString("webview");
        android.webkit.WebView mywebview = findViewById(R.id.webView);
        mywebview.getSettings().setJavaScriptEnabled(true);
        mywebview.getSettings().setGeolocationEnabled(true);
        dialog= new ProgressDialog(WebView.this);
        mywebview.setBackgroundColor(getResources().getColor(R.color.greyish));
        mywebview.getSettings().setSupportZoom(true);
        //mywebview.getSettings().setBuiltInZoomControls(true);
        mywebview.getSettings().setLoadWithOverviewMode(true);
        mywebview.getSettings().setUseWideViewPort(true);
        showProgressDialog();
        mywebview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(android.webkit.WebView view, String url) {
                dismissProgressDialog();
            }
        });
        mywebview.setWebChromeClient(new WebChromeClient(){

            @Override
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
            }
        });
        try {
            mywebview.loadUrl(webviewlink);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}


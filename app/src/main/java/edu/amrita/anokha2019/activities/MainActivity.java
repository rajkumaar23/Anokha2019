package edu.amrita.anokha2019.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.helpers.GlobalData;
import edu.amrita.anokha2019.fragments.AboutFragment;
import edu.amrita.anokha2019.fragments.DashboardFragment;
import edu.amrita.anokha2019.fragments.HomeFragment;

public class MainActivity extends AppCompatActivity {



    HomeFragment home=new HomeFragment();
    Fragment fragment=home;
    DashboardFragment dashboard=new DashboardFragment();
    AboutFragment about=new AboutFragment();
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment=home;
                    break;
                case R.id.navigation_dashboard:
                    fragment=dashboard;
                    break;
                case R.id.navigation_about:
                    fragment=about;
                    break;

            }
            return loadFragment(fragment);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadFragment(fragment);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setHapticFeedbackEnabled(true);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }
    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft=
                    getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            ft.replace(R.id.fragment_container, fragment).commit();
            return true;
        }
        return false;
    }

    @Override
    protected void onPostResume() {
        if(!GlobalData.checkSignIn())
        startActivity(new Intent(this,SignInActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        super.onPostResume();
    }
}

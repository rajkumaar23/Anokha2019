package edu.amrita.anokha2019.activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import edu.amrita.anokha2019.R;

public class PrivacyPolicy extends AppCompatActivity {
    TextView t;
    String s = "WELCOME TO OUR PRIVACY POLICY\n" +
            "Your privacy is critically important to us.\n" +
            "\n" +
            "Anokha is located at:\n" +
            "Anokha\n" +
            "Amritanagar, Ettimadai\n" +
            "Coimbatore, Tamil Nadu - 641112.\n" +
            "Ph No: 8508218330\n" +
            "\n" +
            "It is Anokha’s policy to respect your privacy regarding any information we may collect while operating our website. This Privacy Policy applies to https://anokha.amrita.edu/ (hereinafter, \"us\", \"we\", or \"https://anokha.amrita.edu/\"). We respect your privacy and are committed to protecting personally identifiable information you may provide us through the Website. We have adopted this privacy policy (\"Privacy Policy\") to explain what information may be collected on our Website, how we use this information, and under what circumstances we may disclose the information to third parties. This Privacy Policy applies only to information we collect through the Website and does not apply to our collection of information from other sources.\n" +
            "\n" +
            "This Privacy Policy, together with the Terms and conditions posted on our Website, set forth the general rules and policies governing your use of our Website. Depending on your activities when visiting our Website, you may be required to agree to additional terms and conditions.\n" +
            "\n" +
            "Website Visitors\n" +
            "\n" +
            "Like most website operators, Anokha collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. Anokha’s purpose in collecting non-personally identifying information is to better understand how Anokha’s visitors use its website. From time to time, Anokha may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its website.\n" +
            "\n" +
            "Anokha also collects potentially personally-identifying information like Internet Protocol (IP) addresses for logged in users and for users leaving comments on https://anokha.amrita.edu/ blog posts. Anokha only discloses logged in user and commenter IP addresses under the same circumstances that it uses and discloses personally-identifying information as described below.\n" +
            "\n" +
            " \n" +
            "\n" +
            "Gathering of Personally-Identifying Information\n" +
            "\n" +
            "Certain visitors to Anokha’s websites choose to interact with Anokha in ways that require Anokha to gather personally-identifying information. The amount and type of information that Anokha gathers depends on the nature of the interaction. For example, we ask visitors who sign up for a blog at https://anokha.amrita.edu/ to provide a username and email address.\n" +
            "\n" +
            " \n" +
            "\n" +
            "Security\n" +
            "\n" +
            "The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.\n" +
            "\n" +
            " \n" +
            "\n" +
            "Links To External Sites\n" +
            "\n" +
            "Our Service may contain links to external sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy and terms and conditions of every site you visit.\n" +
            "\n" +
            "We have no control over, and assume no responsibility for the content, privacy policies or practices of any third party sites, products or services.\n" +
            "\n" +
            "Aggregated Statistics\n" +
            "\n" +
            "Anokha may collect statistics about the behavior of visitors to its website. Anokha may display this information publicly or provide it to others. However, Anokha does not disclose your personally-identifying information.\n" +
            "\n" +
            " \n" +
            "\n" +
            "Privacy Policy Changes\n" +
            "\n" +
            "Although most changes are likely to be minor, Anokha may change its Privacy Policy from time to time, and in Anokha’s sole discretion. Anokha encourages visitors to frequently check this page for any changes to its Privacy Policy. Your continued use of this site after any change in this Privacy Policy will constitute your acceptance of such change.\n\n";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        android.support.v7.widget.Toolbar toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Privacy Policy");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        t = (TextView)findViewById(R.id.privacytext);
        t.setText(s);


    }
}

package edu.amrita.anokha2019.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.AsyncTask;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.helpers.GlobalData;
import edu.amrita.anokha2019.helpers.FingerprintHandler;
import edu.amrita.anokha2019.helpers.SSLPin;
import edu.amrita.anokha2019.helpers.checkNetwork;
import edu.amrita.anokha2019.helpers.okLogin;
import okhttp3.CertificatePinner;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SignInActivity extends AppCompatActivity {

    EditText usernameEdittext,passwordEdittext;
    String username,password;
    Button login,signup;
    CheckBox box;
    String rmpassword = "";
    String rmusername = "";
    TextView message;
    String url_login,res;
    ProgressDialog dialog;
    Response response;
    CertificatePinner certificatePinner;
    AlertDialog dialogg;
    Dialog dj;


    private static final String KEY_NAME = "securel0g1n";
    private Cipher cipher;
    private KeyStore keyStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        if(GlobalData.checkSignIn()) {
            finish();
            startActivity(new Intent(SignInActivity.this, MainActivity.class));
        }
        SharedPreferences pref = getSharedPreferences("user", Context.MODE_PRIVATE) ;
        usernameEdittext = (EditText)findViewById(R.id.username);
        passwordEdittext = (EditText)findViewById(R.id.password);
        passwordEdittext.setMaxLines(1);
        login = (Button)findViewById(R.id.login);
        signup=findViewById(R.id.gotosignup);
        box = (CheckBox)findViewById(R.id.checkBox1);
        message=findViewById(R.id.login_title);
        url_login=getResources().getString(R.string.url_login);
        TextView forgotPassword = findViewById(R.id.forgotPassword);

        rmusername = pref.getString("username",null);
        rmpassword = pref.getString("password",null);

        usernameEdittext.setText(rmusername);
        passwordEdittext.setText(rmpassword);
        if(!TextUtils.isEmpty(usernameEdittext.getText().toString())){
            box.setChecked(true);
        }
        try {
            usernameEdittext.setSelection(rmusername.length());
            passwordEdittext.setSelection(rmpassword.length());
        }catch (NullPointerException e){
            e.printStackTrace();
        }

        if(rmusername!=null && rmpassword!=null && pref.getBoolean("fingerprint",true)){
            showFingerPrintDialog();
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                if (checkifedittextEmpty() && emailValidation()){
                    if(checkNetwork.isConnected(SignInActivity.this)){
                            username = usernameEdittext.getText().toString();
                            password = passwordEdittext.getText().toString();
                            new okLogin(SignInActivity.this, username, password, box, url_login).execute();
                       }else{
                        Toast.makeText(SignInActivity.this,"Please connect to internet",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignInActivity.this,SignUpActivity.class));
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    dj = new Dialog(SignInActivity.this);
                    dj.setContentView(R.layout.forgotpassword);
                    final EditText ed = (EditText) dj.findViewById(R.id.email);
                    Button bt = (Button) dj.findViewById(R.id.submit);
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            hideKeyboard();
                            if (checkNetwork.isConnected(SignInActivity.this)) {
                                new resetPassword().execute(ed.getText().toString());
                            } else {
                                Toast.makeText(SignInActivity.this, "Please connect to internet", Toast.LENGTH_LONG).show();
                            }

                        }
                    });
                    dj.show();
                }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode){
            case 2304 :
                dj = new Dialog(SignInActivity.this);
                dj.setContentView(R.layout.forgotpassword);
                final EditText ed = (EditText) dj.findViewById(R.id.email);
                Button bt = (Button) dj.findViewById(R.id.submit);
                bt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideKeyboard();
                        if (checkNetwork.isConnected(SignInActivity.this)) {
                            new resetPassword().execute(ed.getText().toString());
                        } else {
                            Toast.makeText(SignInActivity.this, "Please connect to internet", Toast.LENGTH_LONG).show();
                        }

                    }
                });
                dj.show();
                break;
        }
    }

    @SuppressLint("StaticFieldLeak")
    class resetPassword extends AsyncTask<String,Void,Void>{
        String email;
        @Override
        protected void onPreExecute() {
            if(dj.isShowing())
                dj.dismiss();
            dialog=new ProgressDialog(SignInActivity.this);
            dialog.setMessage("Please wait");
            dialog.setCancelable(false);
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            try{
                certificatePinner=SSLPin.getCertPinner(SignInActivity.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();

                RequestBody body = new FormBody.Builder()
                        .add("email",strings[0])
                        .build();
                email=strings[0];

                Request request = new Request.Builder()
                        .url("https://anokha.amrita.edu/api/recover")
                        .addHeader("device","android")
                        .post(body)
                        .build();
                response=client.newCall(request).execute();
                assert response.body() != null;
                res=response.body().string();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if(dialog.isShowing())
                dialog.dismiss();
            try {
                JSONObject response = new JSONObject(res);
                if(response.getBoolean("success")) {
                    startActivity(new Intent(SignInActivity.this, OTPVerifyActivity.class).putExtra("email",email));
                }
                else{
                    Dialog dew = new Dialog(SignInActivity.this);
                    dew.setContentView(R.layout.dialog_regsuccessful);
                    TextView t = (TextView)dew.findViewById(R.id.textmsg);
                    t.setText(response.getString("error"));
                    dew.show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            super.onPostExecute(aVoid);
        }
    }

    void showFingerPrintDialog(){
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.fingerprint_dialog_content, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(alertLayout);
        dialogg = alert.create();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try { //Get an instance of KeyguardManager and FingerprintManager//
                KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
                FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                TextView textView = (TextView) findViewById(R.id.fingerprint_status);
                boolean flag = true;

                if (fingerprintManager == null)
                    return;
                //Check whether the device has a fingerprint sensor//
                if (!fingerprintManager.isHardwareDetected()) {
                    // If a fingerprint sensor isn’t available, then inform the user that they’ll be unable to use your app’s fingerprint functionality//
                    flag = false;
                }
                //Check whether the user has granted your app the USE_FINGERPRINT permission//
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                    // If your app doesn't have this permission, then display the following text//
                    flag = false;

                }

                //Check that the user has registered at least one fingerprint//
                if (!fingerprintManager.hasEnrolledFingerprints()) {
                    // If the user hasn’t configured any fingerprints, then display the following message//
                    flag = false;
                }

                //Check that the lockscreen is secured//
                if (!keyguardManager.isKeyguardSecure()) {
                    // If the user hasn’t secured their lockscreen with a PIN password or pattern, then display the following text//

                    flag = false;
                }
                if (flag) {
                    try {
                        generateKey();
                    } catch (FingerprintException e) {
                        e.printStackTrace();
                    }

                    final FingerprintHandler helper;
                    if (initCipher()) {
                        dialogg.show();
                        //If the cipher is initialized successfully, then create a CryptoObject instance//
                        FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                        username = usernameEdittext.getText().toString();
                        password = passwordEdittext.getText().toString();
                        helper = new FingerprintHandler(this, username, password, box, url_login, dialogg);
                        helper.startAuth(fingerprintManager, cryptoObject);
                        dialogg.findViewById(R.id.cancel_action).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogg.dismiss();
                                helper.stopListening();


                            }
                        });
                    }
                }

            }catch(NullPointerException e){
                e.printStackTrace();
            }

        }
    }

    public void hideKeyboard(){
        try {
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputManager.isAcceptingText())
                inputManager.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
        }catch (Exception e){
            e.printStackTrace();
            Crashlytics.log(e.getMessage());
        }
    }

    public boolean checkifedittextEmpty() {
        if (TextUtils.isEmpty(usernameEdittext.getText().toString())) {
            usernameEdittext.setError("This field cannot be empty");
            YoYo.with(Techniques.Shake).duration(100).repeat(1).playOn(usernameEdittext);
            usernameEdittext.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
            return false;
        }
        if (TextUtils.isEmpty(passwordEdittext.getText().toString())){
            passwordEdittext.setError("This field cannot be empty");
            //password.setHint("Please enter your password");
            passwordEdittext.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
            YoYo.with(Techniques.Shake).duration(100).repeat(1).playOn(passwordEdittext);
            return false;

        }
        return true;
    }

    public boolean emailValidation(){
        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern p = Pattern.compile(EMAIL_STRING);
        Matcher check = p.matcher(usernameEdittext.getText().toString());
        Boolean b = check.matches();
        if (!b){
            usernameEdittext.setError("Enter valid email address");
            return false;
        }else{
            return  true;
        }
    }




    private void generateKey() throws FingerprintException {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                // Obtain a reference to the Keystore using the standard Android keystore container identifier (“AndroidKeystore”)//
                keyStore = KeyStore.getInstance("AndroidKeyStore");

                //Generate the key//
                KeyGenerator keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

                //Initialize an empty KeyStore//
                keyStore.load(null);

                //Initialize the KeyGenerator//
                keyGenerator.init(new

                        //Specify the operation(s) this key can be used for//
                        KeyGenParameterSpec.Builder(KEY_NAME,
                        KeyProperties.PURPOSE_ENCRYPT |
                                KeyProperties.PURPOSE_DECRYPT)
                        .setBlockModes(KeyProperties.BLOCK_MODE_CBC)

                        //Configure this key so that the user has to confirm their identity with a fingerprint each time they want to use it//
                        .setUserAuthenticationRequired(true)
                        .setEncryptionPaddings(
                                KeyProperties.ENCRYPTION_PADDING_PKCS7)
                        .build());

                //Generate the key//
                keyGenerator.generateKey();

            } catch (KeyStoreException
                    | NoSuchAlgorithmException
                    | NoSuchProviderException
                    | InvalidAlgorithmParameterException
                    | CertificateException
                    | IOException exc) {
                exc.printStackTrace();
                throw new FingerprintException(exc);
            }
        }


    }

    //Create a new method that we’ll use to initialize our cipher//
    public boolean initCipher() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                //Obtain a cipher instance and configure it with the properties required for fingerprint authentication//
                cipher = Cipher.getInstance(
                        KeyProperties.KEY_ALGORITHM_AES + "/"
                                + KeyProperties.BLOCK_MODE_CBC + "/"
                                + KeyProperties.ENCRYPTION_PADDING_PKCS7);
            } catch (NoSuchAlgorithmException |
                    NoSuchPaddingException e) {
                throw new RuntimeException("Failed to get Cipher", e);
            }

            try {
                keyStore.load(null);
                SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                        null);
                cipher.init(Cipher.ENCRYPT_MODE, key);
                //Return true if the cipher has been initialized successfully//
                return true;
            } catch (KeyPermanentlyInvalidatedException e) {

                //Return false if cipher initialization failed//
                return false;
            } catch (KeyStoreException | CertificateException
                    | UnrecoverableKeyException | IOException
                    | NoSuchAlgorithmException | InvalidKeyException e) {
                throw new RuntimeException("Failed to init Cipher", e);
            }
        }else{
            return false;
        }
    }

    private class FingerprintException extends Exception {
        FingerprintException(Exception e) {
            super(e);
        }
    }
}

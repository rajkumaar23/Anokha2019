package edu.amrita.anokha2019.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.helpers.GlobalData;
import edu.amrita.anokha2019.helpers.SSLPin;
import edu.amrita.anokha2019.helpers.Utils;
import edu.amrita.anokha2019.models.Event;
import edu.amrita.anokha2019.models.Workshop;
import okhttp3.CertificatePinner;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisteredWorkshops extends AppCompatActivity {

    CertificatePinner certificatePinner;
    ProgressBar progressBar;
    Response response;
    String url_reg_events,url_login,res;
    ArrayList<String> workshops;
    ArrayAdapter<String> workshopAdapter;
    JSONObject workshopJSON,workshopsAll;
    ListView listView;
    String url_workshops,url_reg_workshops;
    ArrayList<Workshop> workshopsList,regWorkshops;

    private static final Random RANDOM = new Random();
    int[] mMaterialColors;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registered_workshops);
        Toolbar toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Registered Workshops");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        url_reg_workshops=getResources().getString(R.string.url_reg_workshops);
        url_workshops=getResources().getString(R.string.url_workshops);
        progressBar=findViewById(R.id.spin_kit);
        url_login=getResources().getString(R.string.url_login);
        mMaterialColors = getResources().getIntArray(R.array.colors);
        workshops=new ArrayList<>();
        workshopsList=new ArrayList<>();
        regWorkshops=new ArrayList<>();
        listView=findViewById(R.id.list);

        new fetchData().execute();
    }

    @SuppressLint("StaticFieldLeak")
    class fetchData extends AsyncTask<Void,Void,Void>{
        String name,teamcode,paidby;
        @Override
        protected void onPreExecute() {
            progressBar.getIndeterminateDrawable().setColorFilter(mMaterialColors[RANDOM.nextInt(mMaterialColors.length)],android.graphics.PorterDuff.Mode.SRC_IN);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                certificatePinner=SSLPin.getCertPinner(RegisteredWorkshops.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();

                Request request;

                //Refreshing token
                if(Utils.isTokenExpired()) {
                    RequestBody body = new FormBody.Builder()
                            .add("email", GlobalData.username)
                            .add("password", GlobalData.password)
                            .build();

                    request = new Request.Builder()
                            .url(url_login)
                            .post(body)
                            .build();
                    response = client.newCall(request).execute();
                    res = response.body().string();

                    try {
                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getBoolean("success")) {
                            JSONObject data = jsonObject.getJSONObject("data");
                            GlobalData.token = data.getString("token");
                        }
                    } catch (Exception e) {
                        //Toast.makeText(RegisteredEvents.this,"Error connecting to server",Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }

                //Getting Registered Workshops
                request = new Request.Builder()
                        .url(url_reg_workshops)
                        .addHeader("Authorization","Bearer "+GlobalData.token)
                        .get()
                        .build();
                response=client.newCall(request).execute();
                assert response.body() != null;
                workshopJSON=new JSONObject(response.body().string());
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            try{
                try {
                    JSONArray jsonArray = workshopJSON.getJSONArray("data");
                    if (workshopJSON.getBoolean("success")) {
                        for (int i = 0; i < jsonArray.length(); ++i) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            Workshop workshop = new Workshop();
                            workshop.setPaidby(jsonObject.getString("paidby"));
                            workshop.setTeamcode(jsonObject.getString("teamcode"));
                            workshop.setName(Utils.toTitleCase(jsonObject.getString("name")));
                            regWorkshops.add(workshop);
                            workshops.add(workshop.getName());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    finish();
                    Toast.makeText(RegisteredWorkshops.this, "Unexpected error", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(RegisteredWorkshops.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
                }
                listView.setEmptyView(findViewById(R.id.empty_workshops));
                workshopAdapter = new ArrayAdapter<>(RegisteredWorkshops.this,android.R.layout.simple_list_item_1,workshops);
                listView.setAdapter(workshopAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        final Workshop workshop = regWorkshops.get(position);
                        Dialog dew = new Dialog(RegisteredWorkshops.this);
                        dew.setContentView(R.layout.dialog_regsuccessful);
                        TextView t = (TextView)dew.findViewById(R.id.textmsg);
                        t.setText(workshop.getName()+"\n\nTeam Code : "+workshop.getTeamcode()+"\nPaid By : "+workshop.getPaidby());
                        dew.show();
                    }
                });
            }catch (Exception e){
                finish();
                Toast.makeText(RegisteredWorkshops.this,"Some error occured.",Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            super.onPostExecute(aVoid);
        }
    }
}

package edu.amrita.anokha2019.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.helpers.GlobalData;
import edu.amrita.anokha2019.helpers.SSLPin;
import edu.amrita.anokha2019.helpers.Utils;
import edu.amrita.anokha2019.models.Event;
import edu.amrita.anokha2019.models.Workshop;
import okhttp3.CertificatePinner;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisteredEvents extends AppCompatActivity {

    CertificatePinner certificatePinner;
    ProgressBar progressBar;
    Response response;
    String url_reg_events,url_login,res;
    ArrayList<String> events;
    ArrayAdapter<String> eventAdapter;
    JSONObject eventJSON,eventsAll;
    ListView listView;
    String url_events;
    ArrayList<Event> eventsList,regEvents;

    private static final Random RANDOM = new Random();
    int[] mMaterialColors;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registered_events);
        Toolbar toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Registered Events");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        url_reg_events=getResources().getString(R.string.url_reg_events);
        url_events=getResources().getString(R.string.url_events);
        progressBar=findViewById(R.id.spin_kit);
        url_login=getResources().getString(R.string.url_login);
        mMaterialColors = getResources().getIntArray(R.array.colors);
        events=new ArrayList<>();
        eventsList=new ArrayList<>();
        regEvents=new ArrayList<>();
        listView=findViewById(R.id.list);

        new fetchData().execute();
    }

    @SuppressLint("StaticFieldLeak")
    class fetchData extends AsyncTask<Void,Void,Void>{
        @Override
        protected void onPreExecute() {
            progressBar.getIndeterminateDrawable().setColorFilter(mMaterialColors[RANDOM.nextInt(mMaterialColors.length)],android.graphics.PorterDuff.Mode.SRC_IN);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                certificatePinner=SSLPin.getCertPinner(RegisteredEvents.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();
                Request request;

                //Refreshing token
                if(Utils.isTokenExpired()) {
                    RequestBody body = new FormBody.Builder()
                            .add("email", GlobalData.username)
                            .add("password", GlobalData.password)
                            .build();

                    request = new Request.Builder()
                            .url(url_login)
                            .post(body)
                            .build();
                    response = client.newCall(request).execute();
                    assert response.body() != null;
                    res = response.body().string();

                    try {
                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getBoolean("success")) {
                            JSONObject data = jsonObject.getJSONObject("data");
                            GlobalData.token = data.getString("token");
                        }
                    } catch (Exception e) {
                        //Toast.makeText(RegisteredEvents.this,"Error connecting to server",Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }

                //Getting Registered Events
                request = new Request.Builder()
                        .url(url_reg_events)
                        .addHeader("Authorization","Bearer "+GlobalData.token)
                        .get()
                        .build();
                response=client.newCall(request).execute();
                assert response.body() != null;
                eventJSON=new JSONObject(response.body().string());
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            try{
                try {
                    JSONArray jsonArray = eventJSON.getJSONArray("data");
                    if (eventJSON.getBoolean("success")) {
                        for (int i = 0; i < jsonArray.length(); ++i) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            Event event = new Event();
                            event.setPaidby(jsonObject.getString("paidby"));
                            event.setTeamcode(jsonObject.getString("teamcode"));
                            event.setName(Utils.toTitleCase(jsonObject.getString("name")));
                            regEvents.add(event);
                            events.add(event.getName());
                        }
                    }else{
                        finish();
                        Toast.makeText(RegisteredEvents.this, "Unexpected error", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    finish();
                    Toast.makeText(RegisteredEvents.this, "Unexpected error", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(RegisteredEvents.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
                }
                listView.setEmptyView(findViewById(R.id.empty_events));
                eventAdapter = new ArrayAdapter<>(RegisteredEvents.this,android.R.layout.simple_list_item_1,events);
                listView.setAdapter(eventAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        final Event event = regEvents.get(position);
                        Dialog dew = new Dialog(RegisteredEvents.this);
                        dew.setContentView(R.layout.dialog_regsuccessful);
                        TextView t = (TextView)dew.findViewById(R.id.textmsg);
                        t.setText(event.getName()+"\n\nTeam Code : "+event.getTeamcode()+"\nPaid By : "+event.getPaidby());
                        dew.show();
                    }
                });
            }catch (Exception e){
                finish();
                Toast.makeText(RegisteredEvents.this,"Some error occured.",Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            super.onPostExecute(aVoid);
        }
    }
}

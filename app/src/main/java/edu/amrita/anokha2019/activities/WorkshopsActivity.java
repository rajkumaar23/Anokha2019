package edu.amrita.anokha2019.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.adapters.WorkshopListAdapter;
import edu.amrita.anokha2019.helpers.SSLPin;
import edu.amrita.anokha2019.helpers.Utils;
import edu.amrita.anokha2019.models.Workshop;
import okhttp3.CertificatePinner;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class WorkshopsActivity extends AppCompatActivity {

    int dept_id;
    String url_workshops;
    CertificatePinner certificatePinner;
    Response response;
    JSONObject workshops;
    ListView listView;
    ArrayList<Workshop> workshopsList;
    WorkshopListAdapter workshopAdapter;
    ProgressBar progressBar;
    TextView empty_view;
    private static final Random RANDOM = new Random();
    int[] mMaterialColors;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workshops_list);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        try{
            dept_id=getIntent().getExtras().getInt("dept_id");
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        mMaterialColors = getResources().getIntArray(R.array.colors);

        url_workshops=getResources().getString(R.string.url_workshops);
        listView = findViewById(R.id.work_list);
        workshopsList=new ArrayList<>();
        empty_view=findViewById(R.id.empty_events);
        progressBar=findViewById(R.id.spin_kit);
        
        new getWorkshops().execute();
    }


    class getWorkshops extends AsyncTask<Void,Void,Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.getIndeterminateDrawable().setColorFilter(mMaterialColors[RANDOM.nextInt(mMaterialColors.length)],android.graphics.PorterDuff.Mode.SRC_IN);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                certificatePinner=SSLPin.getCertPinner(WorkshopsActivity.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();

                Request request = new Request.Builder()
                        .url(url_workshops)
                        .get()
                        .build();
                response=client.newCall(request).execute();
                workshops=new JSONObject(response.body().string());
            }catch (IOException e){
                e.printStackTrace();
            }catch (JSONException e){
                e.printStackTrace();
            }
            catch (NullPointerException e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            try{
                try {
                    JSONArray jsonArray = workshops.getJSONArray("data");
                    if (workshops.getBoolean("success")) {
                        for (int i = 0; i < jsonArray.length(); ++i) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            if(jsonObject.getInt("dep_id")==dept_id) {
                                String title=Utils.toTitleCase(jsonObject.getString("name"));
                                String subtitle=jsonObject.getString("subtitle").equals("null") ? null : Utils.toTitleCase(jsonObject.getString("subtitle"));
                                int id=jsonObject.getInt("id");
                                Workshop work = new Workshop(title,subtitle ,id );
                                workshopsList.add(work);
                            }
                        }
                        workshopAdapter = new WorkshopListAdapter(WorkshopsActivity.this,workshopsList);
                        listView.setAdapter(workshopAdapter);
                        listView.setEmptyView(empty_view);
                    }
                } catch (Exception e) {
                    if(workshops.has("error")){
                        try {
                            Toast.makeText(WorkshopsActivity.this,workshops.getString("error"),Toast.LENGTH_LONG).show();
                            finish();
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }else {
                        e.printStackTrace();
                        Toast.makeText(WorkshopsActivity.this, "Unexpected error", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(WorkshopsActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                }
            }catch (Exception e){
                Toast.makeText(WorkshopsActivity.this,"Some error occured.",Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            super.onPostExecute(aVoid);
        }
    }
}

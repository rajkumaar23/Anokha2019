package edu.amrita.anokha2019.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.adapters.DeptListAdapter;
import edu.amrita.anokha2019.helpers.SSLPin;
import edu.amrita.anokha2019.models.Department;
import okhttp3.CertificatePinner;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DepartmentsActivity extends AppCompatActivity {

    JSONObject depts;
    ListView listView;
    ArrayList<Department> deptList;
    DeptListAdapter deptAdapter;
    CertificatePinner certificatePinner;
    ProgressBar progressBar;
    Response response;
    private static final Random RANDOM = new Random();
    int[] mMaterialColors;
    int choice;
    String url_dept;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_departments);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Departments");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        choice=getIntent().getExtras().getInt("choice");
        listView = findViewById(R.id.dept_list);
        progressBar=findViewById(R.id.spin_kit);

        mMaterialColors = getResources().getIntArray(R.array.colors);
        url_dept=getResources().getString(R.string.url_departments);
        deptList=new ArrayList<>();
        new retrieveDepartments().execute();

    }

    @SuppressLint("StaticFieldLeak")
    class retrieveDepartments extends AsyncTask<Void,Void,Void>{

        @Override
        protected void onPreExecute() {
            progressBar.getIndeterminateDrawable().setColorFilter(mMaterialColors[RANDOM.nextInt(mMaterialColors.length)],android.graphics.PorterDuff.Mode.SRC_IN);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                certificatePinner=SSLPin.getCertPinner(DepartmentsActivity.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();

                Request request = new Request.Builder()
                        .url(url_dept)
                        .get()
                        .build();
                response=client.newCall(request).execute();
                depts=new JSONObject(response.body().string());
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            try{
                try {
                    JSONArray jsonArray = depts.getJSONArray("data");
                    if (depts.getBoolean("success")) {
                        for (int i = 0; i < jsonArray.length(); ++i) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            Department dept=new Department(jsonObject.getString("name").toUpperCase(),jsonObject.getInt("id"));
                            deptList.add(dept);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(DepartmentsActivity.this, "Unexpected error", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(DepartmentsActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
                }
                deptAdapter = new DeptListAdapter(DepartmentsActivity.this,deptList,choice);
                listView.setAdapter(deptAdapter);
                listView.setEmptyView(findViewById(R.id.empty));
            }catch (Exception e){
                Toast.makeText(DepartmentsActivity.this,"Some error occured.",Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            super.onPostExecute(aVoid);
        }
    }


}

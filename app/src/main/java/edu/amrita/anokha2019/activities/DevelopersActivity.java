package edu.amrita.anokha2019.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import edu.amrita.anokha2019.R;

public class DevelopersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developers);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        String baseURL="https://raw.githubusercontent.com/narayanramn2r/anokha19assets/master/crew/wmd/";
        Picasso.get().load(baseURL+"rajkumar.jpg").placeholder(R.drawable.user).into((ImageView)findViewById(R.id.rajkumar));
        Picasso.get().load(baseURL+"clement.jpg").placeholder(R.drawable.user).into((ImageView)findViewById(R.id.clement));
        Picasso.get().load(baseURL+"n2r.jpg").placeholder(R.drawable.user).into((ImageView)findViewById(R.id.n2r));
        Picasso.get().load(baseURL+"nivedh.jpg").placeholder(R.drawable.user).into((ImageView)findViewById(R.id.niveth));
        Picasso.get().load(baseURL+"kogulkarnakv.jpg").placeholder(R.drawable.user).into((ImageView)findViewById(R.id.kogul));
        Picasso.get().load(baseURL+"akash.jpg").placeholder(R.drawable.user).into((ImageView)findViewById(R.id.akash));
        Picasso.get().load(baseURL+"kailash.jpg").placeholder(R.drawable.user).into((ImageView)findViewById(R.id.kailash));
        Picasso.get().load(baseURL+"harshit.jpg").placeholder(R.drawable.user).into((ImageView)findViewById(R.id.harshit));
        Picasso.get().load(baseURL+"jagan.JPG").placeholder(R.drawable.user).into((ImageView)findViewById(R.id.jagan));
        Picasso.get().load(baseURL+"praga.jpg").placeholder(R.drawable.user).into((ImageView)findViewById(R.id.praga));
        Picasso.get().load(baseURL+"pranesh.jpg").placeholder(R.drawable.user).into((ImageView)findViewById(R.id.pranesh));
        Picasso.get().load(baseURL+"vishvesh.jpg").placeholder(R.drawable.user).into((ImageView)findViewById(R.id.vishvesh));
        Picasso.get().load(baseURL+"subash.jpg").placeholder(R.drawable.user).into((ImageView)findViewById(R.id.subash));



    }
}

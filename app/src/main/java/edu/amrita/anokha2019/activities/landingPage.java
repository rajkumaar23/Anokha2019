package edu.amrita.anokha2019.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;

import org.json.JSONObject;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.helpers.GlobalData;
import edu.amrita.anokha2019.helpers.checkNetwork;
import okhttp3.CertificatePinner;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class landingPage extends AppCompatActivity {

    Response response;
    CertificatePinner certificatePinner;
    ShimmerFrameLayout container;
    Dialog internet,update;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_page);

        container = findViewById(R.id.shimmer_view_container);
        container.setScrollBarFadeDuration(5);
        container.startShimmer();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                if(checkNetwork.isConnected(landingPage.this))
                    new checkVersion().execute();

                else{
                    container.stopShimmer();
                    internet = new Dialog(landingPage.this);
                    internet.setContentView(R.layout.update_dialog);
                    internet.setCancelable(false);
                    TextView textView = internet.findViewById(R.id.update_text);
                    textView.setText("No internet connection is available \nPlease try again later");
                    Button bt = (Button) internet.findViewById(R.id.okay);
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            internet.cancel();
                            finish();
                        }
                    });
                    internet.show();
                }
            }
        }, 2000);


    }

    class checkVersion extends AsyncTask<Void,Void,Void>{
        String res;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                //certificatePinner=SSLPin.getCertPinner(landingPage.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        //.certificatePinner(certificatePinner)
                        .build();

                Request request = new Request.Builder()
                        .url("https://anokha.amrita.edu/api/android/version")
                        .get()
                        .build();
                response=client.newCall(request).execute();
                res=response.body().string();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String existing_version="1.0";
            try{
                String original_version = new JSONObject(res).getString("version");
                //String original_version="1.0.3";
                GlobalData.version=original_version;
                Log.e("ORIGINAL VERSION",original_version);
                try {
                    PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    existing_version = pInfo.versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                Log.e("EXISTING VERSION",existing_version);
                if(original_version.equals(existing_version)){
                    startActivity(new Intent(landingPage.this,SignInActivity.class));
                    finish();
                }else{
                    container.stopShimmer();
                    update = new Dialog(landingPage.this);
                    update.setContentView(R.layout.update_dialog);
                    update.setCancelable(false);
                    TextView textView = update.findViewById(R.id.update_text);
                    textView.setText("Version "+original_version+" is available. Please update the app");
                    Button bt = (Button) update.findViewById(R.id.okay);
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            update.cancel();
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
                            finish();
                        }
                    });
                    update.show();
                }
            }catch (Exception e){
                container.stopShimmer();
                internet = new Dialog(landingPage.this);
                internet.setContentView(R.layout.update_dialog);
                internet.setCancelable(false);
                TextView textView = internet.findViewById(R.id.update_text);
                textView.setText("An unexpected error occurred. Please try again after sometime.");
                Button bt = (Button) internet.findViewById(R.id.okay);
                bt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        internet.cancel();
                        finish();
                    }
                });
                internet.show();
                e.printStackTrace();
            }

            super.onPostExecute(aVoid);
        }
    }
}

package edu.amrita.anokha2019.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import edu.amrita.anokha2019.R;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Settings");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        SharedPreferences pref = getSharedPreferences("user",Context.MODE_PRIVATE) ;
        final SharedPreferences.Editor ed = pref.edit();
        Switch fingerprint=findViewById(R.id.fingerprint);
        fingerprint.setChecked(pref.getBoolean("fingerprint",true));
        fingerprint.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ed.putBoolean("fingerprint",isChecked);
                ed.apply();
            }
        });

        TextView change=findViewById(R.id.change_password);
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsActivity.this,ChangePasswordActivity.class));
            }
        });
    }
}

package edu.amrita.anokha2019.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import co.intentservice.chatui.ChatView;
import co.intentservice.chatui.models.ChatMessage;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.util.EntityUtils;
import edu.amrita.anokha2019.R;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class BotActivity extends AppCompatActivity {

    ChatView chatView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bot);
        chatView = findViewById(R.id.chat_view);
        android.support.v7.widget.Toolbar toolbar=findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        chatView.setOnSentMessageListener(new ChatView.OnSentMessageListener() {
            @Override
            public boolean sendMessage(ChatMessage chatMessage) {
                String gmsg = chatMessage.getMessage();
                String m = sanitize(gmsg);

                new ChatMessage(gmsg, System.currentTimeMillis(), ChatMessage.Type.SENT);
                if (checknetwork()) {
                    sendtobot(m);
                }
                return true;
            }
        });
    }

    public void sendtobot(final String msg) {
        OkHttpClient client = new OkHttpClient.Builder()
                .callTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30,TimeUnit.SECONDS)
                .build();
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, "data="+msg+"&undefined=");
        Request request = new Request.Builder()
                .url("https://anokha.amrita.edu/bot/api")
                .post(body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("cache-control", "no-cache")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull final IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("ERROR",e.getLocalizedMessage());
                        Toast.makeText(BotActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                try {
                    assert response.body() != null;
                    final JSONObject res = new JSONObject(response.body().string());
                    Log.e("RESPONSE",res.toString());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String reply = null;
                            try {
                                reply = res.getJSONObject("data").getString("reply");
                                ChatMessage msg = new ChatMessage(reply,System.currentTimeMillis(), ChatMessage.Type.RECEIVED);
                                chatView.addMessage(msg);
                            } catch (JSONException e) {
                                Toast.makeText(BotActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }
                    });
                }catch (final Exception e){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(BotActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                    e.printStackTrace();
                }
            }
        });
    }


    public boolean checknetwork(){
        if (isNetworkAvailable()){
            return true;
        }else {
            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.layout), "No internet connection..", Snackbar.LENGTH_LONG);
            snackbar.show();
            return false;
        }
    }



    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public String sanitize(String msg) {
        String finalmsg = "";

        for (int i = 0; i < msg.length(); i++) {
            int temp = (int) msg.charAt(i);
            if (temp >= 65 && temp <= 90) {

                finalmsg = finalmsg + msg.charAt(i);
            } else if (temp >= 97 && temp <= 122) {
                finalmsg = finalmsg + msg.charAt(i);
            } else if (temp >= 48 && temp <= 57) {
                finalmsg = finalmsg + msg.charAt(i);
            } else if (temp == 32) {
                finalmsg = finalmsg + msg.charAt(i);
            }
        }
        return  finalmsg;
    }
}

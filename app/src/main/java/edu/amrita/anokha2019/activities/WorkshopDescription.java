package edu.amrita.anokha2019.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Objects;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.helpers.GlobalData;
import edu.amrita.anokha2019.helpers.SSLPin;
import edu.amrita.anokha2019.helpers.Utils;
import edu.amrita.anokha2019.helpers.checkNetwork;
import okhttp3.CertificatePinner;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class WorkshopDescription extends AppCompatActivity {

    int workshop_id;
    ProgressBar spinKitView;
    ScrollView scrollView;
    String url_workshops,rs,url_login;
    Button register,join;
    TextView title,participation,fee,level,venue,contact1_name,contact1_phone,contact2_name,contact2_phone,description,day1,day2,day3;
    CertificatePinner certificatePinner;
    Response response;
    JSONObject workshop_details;

    String res;
    int userid;
    ProgressDialog dialog;

    boolean buttons;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workshop_description);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Workshop Details");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        workshop_id=Objects.requireNonNull(getIntent().getExtras()).getInt("workshop_id");
        url_workshops=getResources().getString(R.string.url_workshops);
        url_login=getResources().getString(R.string.url_login);
        rs=getResources().getString(R.string.Rs);
        title=findViewById(R.id.workshop_title);
        participation=findViewById(R.id.workshop_participation);
        fee=findViewById(R.id.workshop_fee);
        level=findViewById(R.id.workshop_level);
        venue=findViewById(R.id.workshop_venue);
        contact1_name=findViewById(R.id.workshop_contact1_name);
        contact1_phone=findViewById(R.id.workshop_contact1_phone);
        contact2_name=findViewById(R.id.workshop_contact2_name);
        contact2_phone=findViewById(R.id.workshop_contact2_phone);
        description=findViewById(R.id.workshop_description);
        day1=findViewById(R.id.workshop_day1);
        day2=findViewById(R.id.workshop_day2);
        day3=findViewById(R.id.workshop_day3);
        register=findViewById(R.id.workshop_register);
        join=findViewById(R.id.workshop_join);
        spinKitView=findViewById(R.id.spin_kit);
        scrollView=findViewById(R.id.scroll_view);
        userid=GlobalData.id;


        buttons=getIntent().getExtras().getBoolean("buttons");
        if(!buttons){
            register.setVisibility(View.GONE);
            join.setVisibility(View.GONE);
        }

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog d = new Dialog(WorkshopDescription.this);
                d.setContentView(R.layout.dialog_eventconfirmation);
                Button okay = (Button) d.findViewById(R.id.okay);
                Button cancel = (Button) d.findViewById(R.id.cancel);
                d.show();
                okay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (d.isShowing())
                            d.dismiss();
                        if (checkNetwork.isConnected(WorkshopDescription.this))
                            new registerWorkshop().execute();
                        else
                            Toast.makeText(WorkshopDescription.this, "Please connect to internet ", Toast.LENGTH_SHORT).show();

                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (d.isShowing())
                            d.dismiss();
                    }
                });
            }
        });

        join.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {

                final Dialog email_dialog = new Dialog(WorkshopDescription.this);
                email_dialog.setContentView(R.layout.dialog_join1);
                TextView t = (TextView)email_dialog.findViewById(R.id.texttoshow);
                t.setText("Please enter the email of the user who has paid for the workshop.");
                final EditText email = (EditText)email_dialog.findViewById(R.id.email);
                Button bt = (Button)email_dialog.findViewById(R.id.submitemail) ;
                Button has_teamcode=email_dialog.findViewById(R.id.already_has_teamcode);
                email_dialog.show();
                bt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (email_dialog.isShowing())
                            email_dialog.dismiss();
                        if(checkNetwork.isConnected(WorkshopDescription.this))
                            new join().execute(email.getText().toString());
                        else
                            Toast.makeText(WorkshopDescription.this,"Please connect to internet ",Toast.LENGTH_SHORT).show();
                    }
                });
                has_teamcode.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        email_dialog.dismiss();
                        final Dialog teamcode_dialog = new Dialog(WorkshopDescription.this);
                        teamcode_dialog.setContentView(R.layout.dialog_join2);
                        final EditText teamcode = (EditText) teamcode_dialog.findViewById(R.id.teamcode);
                        Button submit_teamCode = (Button) teamcode_dialog.findViewById(R.id.submitteamcode);
                        teamcode_dialog.show();
                        submit_teamCode.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(checkNetwork.isConnected(WorkshopDescription.this))
                                    new join2().execute(teamcode.getText().toString());
                                else
                                    Toast.makeText(WorkshopDescription.this,"Please connect to internet ",Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
            }
        });


        new getWorkshopData().execute();

    }

    @SuppressLint("StaticFieldLeak")
    private class getWorkshopData extends AsyncTask<Void,Void,Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                certificatePinner=SSLPin.getCertPinner(WorkshopDescription.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();

                Request request = new Request.Builder()
                        .url(url_workshops+"/"+workshop_id)
                        .get()
                        .build();
                response=client.newCall(request).execute();
                workshop_details=new JSONObject(response.body().string());
            }catch (IOException e){
                e.printStackTrace();
            }catch (JSONException e){
                e.printStackTrace();
            }
            catch (NullPointerException e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            try{
                try {
                    JSONObject data = workshop_details.getJSONObject("data");
                    title.setText(data.getString("name"));
                    if(data.getString("fee").toLowerCase().equals("free"))
                        fee.setText(data.getString("fee"));
                    else
                        fee.setText(rs+" "+data.getString("fee"));
                    venue.setText(data.getString("venue").equals("null") ? "Will be updated soon" : data.getString("venue"));
                    participation.setText(data.getInt("maxteamsize")==1 ? "Individual" : data.getInt("minteamsize")+"-"+data.getInt("maxteamsize")+" in a team");
                    level.setText(data.getInt("difficulty")+"/5");
                    description.setText(data.getString("description"));
                    contact1_name.setText(data.getString("manager1_name"));
                    contact1_phone.setText(data.getString("manager1_contact"));
                    contact2_name.setText(data.getString("manager2_name"));
                    contact2_phone.setText(data.getString("manager2_contact"));
                    day1.setText(data.getString("day1").equals("null") ? "N/A" : data.getString("day1"));
                    day2.setText(data.getString("day2").equals("null") ? "N/A" : data.getString("day2"));
                    day3.setText(data.getString("day3").equals("null") ? "N/A" : data.getString("day3"));

                    if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    description.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
                    if(data.getInt("maxteamsize")>1 && buttons)
                        join.setVisibility(View.VISIBLE);



                } catch (Exception e) {
                    e.printStackTrace();
                    finish();
                    Toast.makeText(WorkshopDescription.this, "Unexpected error", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(WorkshopDescription.this, MainActivity.class));
                }
            }catch (Exception e){
                Toast.makeText(WorkshopDescription.this,"Some error occured.",Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            spinKitView.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);

            super.onPostExecute(aVoid);
        }
    }


    @SuppressLint("StaticFieldLeak")
    class registerWorkshop extends AsyncTask<Void,Void,Void>{
        @Override
        protected void onPreExecute() {

            dialog=new ProgressDialog(WorkshopDescription.this);
            dialog.setMessage("Registering ");
            dialog.setCancelable(false);
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                certificatePinner=SSLPin.getCertPinner(WorkshopDescription.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();

                RequestBody body = new FormBody.Builder()
                        .add("email",GlobalData.username)
                        .add("password",GlobalData.password)
                        .build();

                Request request = new Request.Builder()
                        .url("https://anokha.amrita.edu/api/login")
                        .post(body)
                        .build();
                response=client.newCall(request).execute();
                res=response.body().string();
                String jwt=new JSONObject(res).getJSONObject("data").getString("token");

                body=new FormBody.Builder()
                        .add("workshopid",""+workshop_id)
                        .add("userid",""+userid)
                        .build();

                request=new Request.Builder()
                        .url("https://anokha.amrita.edu/api/registration/workshop/register")
                        .addHeader("Authorization","Bearer "+jwt)
                        .post(body)
                        .build();
                response=client.newCall(request).execute();
                assert response.body() != null;
                res=response.body().string();

            }catch (Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(dialog.isShowing())
                dialog.dismiss();
            try {
                JSONObject j = new JSONObject(res);
                if (j.getBoolean("success")){

                    if (j.getBoolean("redirect")){
                        Log.e("PAYMENT",j.toString());
                        startActivity(new Intent(WorkshopDescription.this,PaymentWebView.class).putExtra("webview",j.getString("message")));

                    }else{
                        Dialog dew = new Dialog(WorkshopDescription.this);
                        dew.setContentView(R.layout.dialog_regsuccessful);
                        TextView t = (TextView)dew.findViewById(R.id.textmsg);
                        t.setText(j.getString("message"));
                        dew.show();
                    }
                }else{
                    Dialog dew = new Dialog(WorkshopDescription.this);
                    dew.setContentView(R.layout.dialog_regsuccessful);
                    TextView t = (TextView)dew.findViewById(R.id.textmsg);

                    Log.e("EVENT ID ",""+workshop_id);
                    t.setText(j.getString("error"));
                    dew.show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            super.onPostExecute(aVoid);
        }
    }

    @SuppressLint("StaticFieldLeak")
    class join extends AsyncTask<String,String,Void>{
        @Override
        protected void onPreExecute() {
            dialog=new ProgressDialog(WorkshopDescription.this);
            dialog.setMessage("Checking email address..");
            dialog.setCancelable(false);
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            try{
                certificatePinner=SSLPin.getCertPinner(WorkshopDescription.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();
                Request request;
                RequestBody body;

                //Refreshing token
                if(Utils.isTokenExpired()) {
                    body = new FormBody.Builder()
                            .add("email", GlobalData.username)
                            .add("password", GlobalData.password)
                            .build();

                    request = new Request.Builder()
                            .url(url_login)
                            .post(body)
                            .build();
                    response = client.newCall(request).execute();
                    res = response.body().string();

                    try {
                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getBoolean("success")) {
                            JSONObject data = jsonObject.getJSONObject("data");
                            GlobalData.token = data.getString("token");
                        }
                    } catch (Exception e) {
                        //Toast.makeText(RegisteredEvents.this,"Error connecting to server",Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }

                body=new FormBody.Builder()
                        .add("workshopid",""+workshop_id)
                        .add("email",""+strings[0])
                        .build();

                request=new Request.Builder()
                        .url("https://anokha.amrita.edu/api/registration/addteamworkshop")
                        .addHeader("Authorization","Bearer "+GlobalData.token)
                        .post(body)
                        .build();
                response=client.newCall(request).execute();
                res=response.body().string();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(dialog.isShowing())
                dialog.dismiss();
            try{
                JSONObject j = new JSONObject(res);
                if(j.getBoolean("success")) {
                    final Dialog dj = new Dialog(WorkshopDescription.this);
                    dj.setContentView(R.layout.dialog_join2);
                    final EditText ed = (EditText) dj.findViewById(R.id.teamcode);
                    Button bt = (Button) dj.findViewById(R.id.submitteamcode);
                    dj.show();
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(checkNetwork.isConnected(WorkshopDescription.this))
                                new WorkshopDescription.join2().execute(ed.getText().toString());
                            else
                                Toast.makeText(WorkshopDescription.this,"Please connect to internet ",Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    Dialog dew = new Dialog(WorkshopDescription.this);
                    dew.setContentView(R.layout.dialog_regsuccessful);
                    TextView t = (TextView)dew.findViewById(R.id.textmsg);
                    t.setText(j.getString("error"));
                    dew.show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            super.onPostExecute(aVoid);
        }
    }

    @SuppressLint("StaticFieldLeak")
    class join2 extends AsyncTask<String,Void,Void>{
        @Override
        protected void onPreExecute() {
            dialog=new ProgressDialog(WorkshopDescription.this);
            dialog.setMessage("Verifying..");
            dialog.setCancelable(false);
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            try{
                certificatePinner=SSLPin.getCertPinner(WorkshopDescription.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();

                Request request;
                RequestBody body;

                //Refreshing token
                if(Utils.isTokenExpired()) {
                    body = new FormBody.Builder()
                            .add("email", GlobalData.username)
                            .add("password", GlobalData.password)
                            .build();

                    request = new Request.Builder()
                            .url(url_login)
                            .post(body)
                            .build();
                    response = client.newCall(request).execute();
                    res = response.body().string();

                    try {
                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getBoolean("success")) {
                            JSONObject data = jsonObject.getJSONObject("data");
                            GlobalData.token = data.getString("token");
                        }
                    } catch (Exception e) {
                        //Toast.makeText(RegisteredEvents.this,"Error connecting to server",Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
                body=new FormBody.Builder()
                        .add("teamcode",""+strings[0])
                        .build();

                request=new Request.Builder()
                        .url("https://anokha.amrita.edu/api/registration/responseteamworkshop")
                        .addHeader("Authorization","Bearer "+GlobalData.token)
                        .post(body)
                        .build();
                response=client.newCall(request).execute();
                res=response.body().string();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(dialog.isShowing())
                dialog.dismiss();
            Dialog dew = new Dialog(WorkshopDescription.this);
            dew.setContentView(R.layout.dialog_regsuccessful);
            try {
                JSONObject j = new JSONObject(res);
                if (j.getBoolean("success")){
                    TextView t = (TextView)dew.findViewById(R.id.textmsg);
                    t.setText(j.getString("message"));
                    dew.show();
                }else{
                    TextView t = (TextView)dew.findViewById(R.id.textmsg);
                    t.setText(j.getString("error"));
                    dew.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(aVoid);
        }
    }
}

package edu.amrita.anokha2019.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import edu.amrita.anokha2019.R;

public class SponsorsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsors);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        String baseURL="https://raw.githubusercontent.com/narayanramn2r/anokha19assets/master/sponcers/";
        Picasso.get().load(baseURL+"suryan.jpg").placeholder(R.drawable.io_logo).into((ImageView)findViewById(R.id.suryan));
        Picasso.get().load(baseURL+"british.jpg").placeholder(R.drawable.io_logo).into((ImageView)findViewById(R.id.british));
        Picasso.get().load(baseURL+"dinamalar.jpg").placeholder(R.drawable.io_logo).into((ImageView)findViewById(R.id.dinamalar));
        Picasso.get().load(baseURL+"fast.jpg").placeholder(R.drawable.io_logo).into((ImageView)findViewById(R.id.fasttrack));
        Picasso.get().load(baseURL+"infoview.jpg").placeholder(R.drawable.io_logo).into((ImageView)findViewById(R.id.infoview));
        Picasso.get().load(baseURL+"souled.jpg").placeholder(R.drawable.io_logo).into((ImageView)findViewById(R.id.souled));
        Picasso.get().load(baseURL+"hrblock.jpg").placeholder(R.drawable.io_logo).into((ImageView)findViewById(R.id.hr));
    }
}

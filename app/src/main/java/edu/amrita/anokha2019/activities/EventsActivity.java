package edu.amrita.anokha2019.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.adapters.EventAdapter;
import edu.amrita.anokha2019.helpers.SSLPin;
import edu.amrita.anokha2019.helpers.Utils;
import edu.amrita.anokha2019.models.Event;
import okhttp3.CertificatePinner;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class EventsActivity extends AppCompatActivity {

    int dept_id;
    CertificatePinner certificatePinner;
    Response response;
    JSONObject events;
    ListView listView;
    ArrayList<Event> eventsList;
    EventAdapter eventAdapter;
    ProgressBar progressBar;
    TextView empty_view;
    private static final Random RANDOM = new Random();
    int[] mMaterialColors;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        try{
            dept_id=Objects.requireNonNull(getIntent().getExtras()).getInt("dept_id");
        }catch (NullPointerException e){
            finish();
            e.printStackTrace();
        }
        listView = findViewById(R.id.event_list);
        eventsList=new ArrayList<>();
        mMaterialColors = getResources().getIntArray(R.array.colors);
        empty_view=findViewById(R.id.empty_events);
        progressBar=findViewById(R.id.spin_kit);

        new getEvents().execute();
    }


    @SuppressLint("StaticFieldLeak")
    class getEvents extends AsyncTask<Void,Void,Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.getIndeterminateDrawable().setColorFilter(mMaterialColors[RANDOM.nextInt(mMaterialColors.length)],android.graphics.PorterDuff.Mode.SRC_IN);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                certificatePinner=SSLPin.getCertPinner(EventsActivity.this);
                OkHttpClient client = new OkHttpClient.Builder()
                        .certificatePinner(certificatePinner)
                        .build();

                Request request = new Request.Builder()
                        .url(getString(R.string.domain)+"/api/event/department/"+dept_id)
                        .get()
                        .build();
                response=client.newCall(request).execute();
                assert response.body() != null;
                events=new JSONObject(response.body().string());
            }catch (IOException | NullPointerException | JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            try{
                try {
                    JSONArray jsonArray = events.getJSONArray("data");
                    if (events.getBoolean("success")) {
                        for (int i = 0; i < jsonArray.length(); ++i) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            if(jsonObject.getInt("dep_id")==dept_id) {
                                String title=Utils.toTitleCase(jsonObject.getString("name"));
                                String subtitle=jsonObject.getString("subtitle").equals("null") ? null : Utils.toTitleCase(jsonObject.getString("subtitle"));
                                int id=jsonObject.getInt("id");
                                Event work = new Event(title,subtitle ,id );
                                eventsList.add(work);
                            }
                        }
                        eventAdapter = new EventAdapter(EventsActivity.this,eventsList);
                        listView.setAdapter(eventAdapter);
                        listView.setEmptyView(empty_view);
                    }
                } catch (Exception e) {
                    if(events.has("error")){
                        try {
                            Toast.makeText(EventsActivity.this,events.getString("error"),Toast.LENGTH_LONG).show();
                            finish();
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }else {
                        e.printStackTrace();
                        Toast.makeText(EventsActivity.this, "Unexpected error", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(EventsActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                }
            }catch (Exception e){
                    Toast.makeText(EventsActivity.this, "Some error occured.", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(EventsActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                    e.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            super.onPostExecute(aVoid);
        }
    }
}

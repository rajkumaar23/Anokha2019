package edu.amrita.anokha2019.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import edu.amrita.anokha2019.R;

public class OpenSourceLicenses extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_source_licenses);
        android.support.v7.widget.Toolbar toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Third Party Libraries");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ArrayList<String> items=new ArrayList<>();
        items.add("OkHttp by Square");
        items.add("Picasso by Square");
        items.add("Material Letter Icon by Ivan Baranov");
        items.add("Float Labelled EditText by Wrapp");
        items.add("SpinKit by YBQ");
        items.add("Shimmer by Facebook");
        items.add("DotsIndicator by Tommy Buonomo");
        items.add("JWT.Decode by Auth0");
        items.add("Android View Animations by Daimajia");
        items.add("Firebase SDK by Google");

        ArrayAdapter<String> adapter=new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,items);

        ListView list=findViewById(R.id.list);
        list.setAdapter(adapter);


    }
}

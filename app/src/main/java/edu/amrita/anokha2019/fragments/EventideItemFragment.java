package edu.amrita.anokha2019.fragments;

import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import edu.amrita.anokha2019.helpers.GlobalData;
import edu.amrita.anokha2019.R;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;
import static edu.amrita.anokha2019.helpers.GlobalData.mediaPlayer;

public class EventideItemFragment extends Fragment {

    int choice;
    boolean playMusic=false;

    public EventideItemFragment(){

    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(mediaPlayer!=null) mediaPlayer.stop();
        View rootView= inflater.inflate(R.layout.eventide_item, container, false);
        TextView name=rootView.findViewById(R.id.speaker_name);
        TextView desc=rootView.findViewById(R.id.speaker_desc);
        ImageView pic=rootView.findViewById(R.id.speaker_pic);
        final ImageView music = rootView.findViewById(R.id.music);
        music.setImageResource(R.drawable.ic_eventide_nomusic);
        try{
            choice=getArguments().getInt("choice");
            name.setText(GlobalData.artistsList.get(choice).getName());
            desc.setText(GlobalData.artistsList.get(choice).getDescription());
            Picasso.get().load(GlobalData.artistsList.get(choice).getImageURL()).into(pic);
            if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                desc.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        }catch (NullPointerException e){
            e.printStackTrace();
        }

        music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mediaPlayer!=null)mediaPlayer.stop();
                if(playMusic) {
                    music.setImageResource(R.drawable.ic_eventide_nomusic);
                    playMusic=false;
                }
                else {
                    mediaPlayer = MediaPlayer.create(getContext(), GlobalData.artistsList.get(choice).getMusicFile());
                    music.setImageResource(R.drawable.ic_eventide_musicon);
                    mediaPlayer.start();
                    playMusic=true;
                }
            }
        });
        return rootView;
    }

}
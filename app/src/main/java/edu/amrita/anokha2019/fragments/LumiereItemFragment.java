package edu.amrita.anokha2019.fragments;


import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.helpers.GlobalData;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;


/**
 * A simple {@link Fragment} subclass.
 */
public class LumiereItemFragment extends Fragment {

    int choice;


    public LumiereItemFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_lumiere_item, container, false);
        TextView name=rootView.findViewById(R.id.speaker_name);
        TextView desig=rootView.findViewById(R.id.speaker_desig);
        TextView desc=rootView.findViewById(R.id.speaker_desc);
        ImageView pic=rootView.findViewById(R.id.speaker_pic);
        try{
            choice=getArguments().getInt("choice");
            name.setText(GlobalData.speakersList.get(choice).getName());
            desig.setText(GlobalData.speakersList.get(choice).getDesignation());
            desc.setText(GlobalData.speakersList.get(choice).getDescription());
            Picasso.get().load(GlobalData.speakersList.get(choice).getImage_url()).into(pic);
            if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                desc.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        }catch (NullPointerException e){
            e.printStackTrace();
        }

        return rootView;
    }

}

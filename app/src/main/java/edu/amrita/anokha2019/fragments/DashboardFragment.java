package edu.amrita.anokha2019.fragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.WriterException;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.activities.WalletActivity;
import edu.amrita.anokha2019.helpers.GlobalData;
import edu.amrita.anokha2019.activities.RegisteredEvents;
import edu.amrita.anokha2019.activities.RegisteredWorkshops;
import edu.amrita.anokha2019.activities.SignInActivity;
import edu.amrita.anokha2019.helpers.checkNetwork;


public class DashboardFragment extends Fragment {


    Button signout;
    CardView regEvents,regWorkshops;
    TextView name,anokhaID,mobile,email,college;
    private TextView lumiere;

    public DashboardFragment() {
        // Required empty public constructor
    }


    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_dashboard, container, false);
        signout=rootView.findViewById(R.id.signout);
        regEvents=rootView.findViewById(R.id.reg_events);
        regWorkshops=rootView.findViewById(R.id.reg_workshops);
        name=rootView.findViewById(R.id.intro_name);
        anokhaID=rootView.findViewById(R.id.intro_id);
        mobile=rootView.findViewById(R.id.intro_mobile);
        email=rootView.findViewById(R.id.intro_email);
        college=rootView.findViewById(R.id.intro_college);
        lumiere=rootView.findViewById(R.id.lumiereID);

        name.setText(GlobalData.name);
        mobile.setText(GlobalData.mobile);
        anokhaID.setText("Anokha ID : "+GlobalData.anokhaID);
        if(GlobalData.lumiereID==null || GlobalData.lumiereID.trim().isEmpty()){
            lumiere.setVisibility(View.GONE);
        }else {
            lumiere.setText("Lumiere ID : " + GlobalData.lumiereID);
        }
        email.setText(GlobalData.email);
        college.setText(GlobalData.college);
        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(getContext());
                }
                builder.setCancelable(false);
                builder.setMessage("Are you sure you want to sign out?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                showSignIn();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();

            }
        });
        regEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkNetwork.isConnected(getContext()))
                startActivity(new Intent(getContext(),RegisteredEvents.class));
            }
        });
        regWorkshops.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkNetwork.isConnected(getContext()))
                startActivity(new Intent(getContext(),RegisteredWorkshops.class));
            }
        });

        ((CardView)rootView.findViewById(R.id.ewallet)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(),WalletActivity.class));
                //Toast.makeText(getContext(),"Pros are handling it. Coming soon.",Toast.LENGTH_LONG).show();
            }
        });
        QRGEncoder qrgEncoder = new QRGEncoder(GlobalData.anokhaID, null, QRGContents.Type.TEXT, 600);
        try {
            // Getting QR-Code as Bitmap
            Bitmap bitmap = qrgEncoder.encodeAsBitmap();
            // Setting Bitmap to ImageView
            ((ImageView)rootView.findViewById(R.id.avatar)).setImageBitmap(bitmap);
        } catch (WriterException e) {
            Log.v("AVATAR", e.toString());
        }



        return rootView;
    }
    public void showSignIn(){
        GlobalData.signedin=false;
        startActivity(new Intent(getContext(),SignInActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

}

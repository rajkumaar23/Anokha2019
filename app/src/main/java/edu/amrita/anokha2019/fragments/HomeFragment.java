package edu.amrita.anokha2019.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.shimmer.ShimmerFrameLayout;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.activities.SettingsActivity;
import edu.amrita.anokha2019.helpers.ShadowTransformer;
import edu.amrita.anokha2019.adapters.CardPagerAdapter;
import edu.amrita.anokha2019.models.CardItem;

public class HomeFragment extends Fragment
{

    private ViewPager mViewPager;

    private CardPagerAdapter mCardAdapter;
    private ShadowTransformer mCardShadowTransformer;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        ShimmerFrameLayout shimmer_container = rootView.findViewById(R.id.shimmer_view_container);
        shimmer_container.setScrollBarFadeDuration(500);
        shimmer_container.startLayoutAnimation();
        mViewPager = (ViewPager) rootView.findViewById(R.id.viewPager);



        mViewPager.setAdapter(mCardAdapter);
        mViewPager.setPageTransformer(false, mCardShadowTransformer);
        mCardAdapter = new CardPagerAdapter();
        mCardAdapter.addCardItem(new CardItem(R.string.events_title,R.drawable.events));
        mCardAdapter.addCardItem(new CardItem(R.string.workshops_title,R.drawable.workshops));
        mCardAdapter.addCardItem(new CardItem(R.string.wallet,R.drawable.wallet));
        mCardAdapter.addCardItem(new CardItem(R.string.eventide_title,R.drawable.eventide));
        mCardAdapter.addCardItem(new CardItem(R.string.lumiere_title,R.drawable.lumiere));
        mCardAdapter.addCardItem(new CardItem(R.string.chatbot,R.drawable.chatbot));
        mCardAdapter.addCardItem(new CardItem(R.string.maps_title,R.drawable.maps));
        //mCardAdapter.addCardItem(new CardItem(R.string.expo_title,R.drawable.expo));
//        mCardAdapter.addCardItem(new CardItem(R.string.bot_title,R.drawable.three));
        mCardShadowTransformer = new ShadowTransformer(mViewPager, mCardAdapter);
        mCardShadowTransformer.enableScaling(true);
        mViewPager.setAdapter(mCardAdapter);
        mViewPager.setPageTransformer(false, mCardShadowTransformer);
        mViewPager.setOffscreenPageLimit(3);

        ImageView settings=rootView.findViewById(R.id.settings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(),SettingsActivity.class));
            }
        });
        return rootView;
    }
}
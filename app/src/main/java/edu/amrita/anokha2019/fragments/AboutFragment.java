package edu.amrita.anokha2019.fragments;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.adapters.About_Adapter;
import edu.amrita.anokha2019.models.About_Item;


public class AboutFragment extends Fragment {

    public AboutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_about, container, false);
        ShimmerFrameLayout shimmer_container = rootView.findViewById(R.id.shimmer_view_container);
        shimmer_container.setScrollBarFadeDuration(5);
        shimmer_container.startLayoutAnimation();

        ListView lv = (ListView) rootView.findViewById(R.id.list);

        ArrayList<About_Item> items=new ArrayList<>();
        items.add(new About_Item("About Anokha",R.drawable.info));
        items.add(new About_Item("Developers",R.drawable.dev));
        items.add(new About_Item("Our Sponsors",R.drawable.sponsors));
        items.add(new About_Item("Contact Us",R.drawable.phone));
       // items.add(new About_Item("Guidelines",R.drawable.guidelines));
        items.add(new About_Item("Privacy Policy",R.drawable.privacy));
        items.add(new About_Item("Open Source Licenses",R.drawable.opensource));

        About_Adapter adapter=new About_Adapter(getContext(),items);
        lv.setAdapter(adapter);

        String existing_version="N/A";

        try {
            PackageInfo pInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
            existing_version = "V "+pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        ((TextView)rootView.findViewById(R.id.version)).setText(existing_version);



        return rootView;
    }
}



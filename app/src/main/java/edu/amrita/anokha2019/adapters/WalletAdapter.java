package edu.amrita.anokha2019.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.ivbaranov.mli.MaterialLetterIcon;
import com.google.zxing.WriterException;

import java.util.ArrayList;
import java.util.Random;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.activities.WalletActivity;
import edu.amrita.anokha2019.helpers.GlobalData;
import edu.amrita.anokha2019.models.WalletTransaction;
import edu.amrita.anokha2019.models.Workshop;
import edu.amrita.anokha2019.activities.WorkshopDescription;
import edu.amrita.anokha2019.helpers.checkNetwork;

public class WalletAdapter extends ArrayAdapter<WalletTransaction> {


    public WalletAdapter(Context context, ArrayList<WalletTransaction> transactions) {
        super(context, 0,transactions);
    }

    @NonNull
    @Override
    public View getView(int position,  @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.wallet_item, parent, false);
        }

        final WalletTransaction current=getItem(position);

        TextView name=listItemView.findViewById(R.id.details);
        assert current != null;
        name.setText(current.getVendor_name());

        TextView amount=listItemView.findViewById(R.id.amount);
        amount.setText(String.format("%s Rs", current.getAmount()));
        try {
            if (Double.parseDouble(current.getAmount()) < 0) {
                amount.setBackgroundColor(getContext().getResources().getColor(R.color.red));
            }else if(current.getDetails().toLowerCase().equals("security deposit")){
                amount.setBackgroundColor(getContext().getResources().getColor(R.color.colorAccent));
            }else{
                amount.setBackgroundColor(getContext().getResources().getColor(R.color.green));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        listItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dew = new Dialog(getContext());
                dew.setContentView(R.layout.wallet_expanded_item);
                ((TextView)dew.findViewById(R.id.amount)).setText(current.getAmount());
                ((TextView)dew.findViewById(R.id.transacted_at)).setText(current.getTransacted_at());
                ((TextView)dew.findViewById(R.id.details)).setText(current.getDetails());
                ((TextView)dew.findViewById(R.id.tid)).setText(current.getTid());
                ((TextView)dew.findViewById(R.id.vendor_name)).setText(current.getVendor_name());
                QRGEncoder qrgEncoder = new QRGEncoder(current.getTid(), null, QRGContents.Type.TEXT, 300);
                try {
                    // Getting QR-Code as Bitmap
                    Bitmap bitmap = qrgEncoder.encodeAsBitmap();
                    // Setting Bitmap to ImageView
                    ((ImageView)dew.findViewById(R.id.tidQR)).setImageBitmap(bitmap);
                } catch (WriterException e) {
                    Log.v("TID-EXCEPTION", e.toString());
                }
                dew.show();
                try {
                    WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
                    lWindowParams.copyFrom(dew.getWindow().getAttributes());
                    lWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    dew.show();
                    dew.getWindow().setAttributes(lWindowParams);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });


        return listItemView;
    }
}

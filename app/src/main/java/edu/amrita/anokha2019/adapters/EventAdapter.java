package edu.amrita.anokha2019.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.github.ivbaranov.mli.MaterialLetterIcon;

import java.util.ArrayList;
import java.util.Random;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.models.Event;
import edu.amrita.anokha2019.activities.EventDescription;
import edu.amrita.anokha2019.helpers.checkNetwork;

public class EventAdapter extends ArrayAdapter<Event> {
    private int[] mMaterialColors;
    private static final Random RANDOM = new Random();

    public EventAdapter(Context context, ArrayList<Event> events) {
        super(context, 0,events);
    }

    @NonNull
    @Override
    public View getView(int position,  @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.event_item, parent, false);
        }

        mMaterialColors = getContext().getResources().getIntArray(R.array.colors);
        final Event current=getItem(position);

        TextView name=listItemView.findViewById(R.id.event_name);
        name.setText(current.getName());

        TextView subtitle=listItemView.findViewById(R.id.event_subtitle);
        if(current.getSubtitle()!=null) {
            subtitle.setVisibility(View.VISIBLE);
            subtitle.setTextColor(getContext().getResources().getColor(android.R.color.white));
            subtitle.setText(current.getSubtitle());
        }else{
            subtitle.setVisibility(View.GONE);
        }
        MaterialLetterIcon icon=listItemView.findViewById(R.id.event_icon);
        icon.setLetter(current.getLetter());
        icon.setShapeColor(mMaterialColors[RANDOM.nextInt(mMaterialColors.length)]);
        Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.ubuntu);
        icon.setLetterTypeface(typeface);

        listItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkNetwork.isConnected(getContext()))
                    getContext().startActivity(new Intent(getContext(),EventDescription.class).putExtra("event_id",current.getId()).putExtra("buttons",true));
            }
        });


        return listItemView;
    }
}

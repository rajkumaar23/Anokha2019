package edu.amrita.anokha2019.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.github.ivbaranov.mli.MaterialLetterIcon;

import java.util.ArrayList;
import java.util.Random;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.models.Department;
import edu.amrita.anokha2019.activities.EventsActivity;
import edu.amrita.anokha2019.activities.WorkshopsActivity;
import edu.amrita.anokha2019.helpers.checkNetwork;

public class DeptListAdapter extends ArrayAdapter<Department> {
    private int choice;
    private int[] mMaterialColors;
    private static final Random RANDOM = new Random();

    public DeptListAdapter(Context context, ArrayList<Department> departments, int choice) {
        super(context, 0,departments);
        this.choice=choice;
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.department_item, parent, false);
        }

        mMaterialColors = getContext().getResources().getIntArray(R.array.colors);
        final Department current=getItem(position);

        TextView name=listItemView.findViewById(R.id.dept_name);
        name.setText(current.getDept_name());

        MaterialLetterIcon icon=listItemView.findViewById(R.id.dept_icon);
        icon.setLetter(current.getLetter());
        Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.ubuntu);
        icon.setLetterTypeface(typeface);
        icon.setShapeColor(mMaterialColors[RANDOM.nextInt(mMaterialColors.length)]);
        listItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkNetwork.isConnected(getContext())){
                   if(choice==0){
                       getContext().startActivity(new Intent(getContext(),EventsActivity.class).putExtra("dept_id",current.getDept_id()));
                   }else if(choice==1){
                       getContext().startActivity(new Intent(getContext(),WorkshopsActivity.class).putExtra("dept_id",current.getDept_id()));
                   }
                }
            }
        });



        return listItemView;

    }
}

package edu.amrita.anokha2019.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import edu.amrita.anokha2019.helpers.GlobalData;
import edu.amrita.anokha2019.fragments.LumiereItemFragment;

public class LumiereAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public LumiereAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }
    @Override
    public int getCount() {
        return GlobalData.speakersList.size();
    }

    @Override
    public Fragment getItem(int i) {
        Bundle bundle=new Bundle();
        switch (i){

            default: {
                bundle.putInt("choice",i);
                LumiereItemFragment fragment=new LumiereItemFragment();
                fragment.setArguments(bundle);
                return fragment;
            }
        }
    }


}
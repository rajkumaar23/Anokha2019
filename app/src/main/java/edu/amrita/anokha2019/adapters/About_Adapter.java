package edu.amrita.anokha2019.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.activities.DevelopersActivity;
import edu.amrita.anokha2019.activities.SponsorsActivity;
import edu.amrita.anokha2019.models.About_Item;
import edu.amrita.anokha2019.activities.OpenSourceLicenses;
import edu.amrita.anokha2019.activities.PrivacyPolicy;
import edu.amrita.anokha2019.activities.WebView;
import edu.amrita.anokha2019.helpers.checkNetwork;

public class About_Adapter extends ArrayAdapter<About_Item> {

    public About_Adapter(Context context, ArrayList<About_Item> items){
        super(context,0,items);
    }
    @NonNull
    @Override
    public View getView(int position,@Nullable View convertView,  @NonNull ViewGroup parent) {
        View listItemView=convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.about_item, parent, false);
        }

        final About_Item current=getItem(position);
        final int pos=position;

        ImageView icon=listItemView.findViewById(R.id.icon);
        icon.setImageResource(current.getRes_id());

        TextView title=listItemView.findViewById(R.id.name);
        title.setText(current.getTitle());
        
        listItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pos == 0) {
                    if(checkNetwork.isConnected(getContext())){
                        String URL="https://www.amrita.edu/school/engineering/coimbatore/anokha";
                        Intent intent=new Intent(getContext(),WebView.class);
                        intent.putExtra("webview",URL);
                        getContext().startActivity(intent);}
                    else {
                        Toast.makeText(getContext(),"Please connect to internet",Toast.LENGTH_SHORT).show();
                    }
                }else if (pos == 1) {
                    if(checkNetwork.isConnected(getContext()))
                        getContext().startActivity(new Intent(getContext(),DevelopersActivity.class));
                    else
                        Toast.makeText(getContext(),"Please connect to internet",Toast.LENGTH_LONG).show();
                }
                else if (pos == 2) {
                    if(checkNetwork.isConnected(getContext())){
                        String URL="https://anokha.amrita.edu/sponsor";
                        Intent intent=new Intent(getContext(),WebView.class);
                        intent.putExtra("webview",URL);
                        getContext().startActivity(intent);}
                    else {
                        Toast.makeText(getContext(),"Please connect to internet",Toast.LENGTH_SHORT).show();
                    }
                }

                else if (pos == 3) {
                    Dialog dialog = new Dialog(getContext());
                    dialog.setContentView(R.layout.dialog_about_contacts);
                    dialog.show();
                }
                else if (pos == 4) {
                    getContext().startActivity(new Intent(getContext(),PrivacyPolicy.class));
                }

                else if (pos == 5) {
                    getContext().startActivity(new Intent(getContext(),OpenSourceLicenses.class));

                }
            }
        });





     return listItemView;
    }
}

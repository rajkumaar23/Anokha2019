package edu.amrita.anokha2019.adapters;
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.activities.BotActivity;
import edu.amrita.anokha2019.activities.DepartmentsActivity;
import edu.amrita.anokha2019.activities.EventideActivity;
import edu.amrita.anokha2019.activities.LumiereActivity;
import edu.amrita.anokha2019.activities.WalletActivity;
import edu.amrita.anokha2019.activities.WebView;
import edu.amrita.anokha2019.helpers.Utils;
import edu.amrita.anokha2019.models.CardItem;
import edu.amrita.anokha2019.helpers.checkNetwork;

public class CardPagerAdapter extends PagerAdapter implements CardAdapter {

    private List<CardView> mViews;
    private List<CardItem> mData;
    private float mBaseElevation;

    public CardPagerAdapter() {
        mData = new ArrayList<>();
        mViews = new ArrayList<>();
    }

    public void addCardItem(CardItem item) {
        mViews.add(null);
        mData.add(item);
    }

    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return mViews.get(position);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.cardview_adapter, container, false);
        container.addView(view);
        bind(mData.get(position), view);
        Button button=view.findViewById(R.id.card_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseActivity(container.getContext(),position);
            }
        });
        CardView cardView = (CardView) view.findViewById(R.id.cardView);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               chooseActivity(container.getContext(),position);
            }
        });

        if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }

        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);
        mViews.set(position, cardView);
        return view;
    }

    private void chooseActivity(Context context,int position){
        if (checkNetwork.isConnected(context)) {
            switch (position) {
                case 0:
                    context.startActivity(new Intent(context, DepartmentsActivity.class).putExtra("choice",0));
                    break;
                case 1:
                    context.startActivity(new Intent(context, DepartmentsActivity.class).putExtra("choice",1));
                    break;
                case 2:
                    context.startActivity(new Intent(context, WalletActivity.class));
                    break;
                case 3:
                    context.startActivity(new Intent(context,EventideActivity.class));
                    break;
                case 4:
                    context.startActivity(new Intent(context,LumiereActivity.class));
                    break;
                case 5:
                    context.startActivity(new Intent(context, BotActivity.class));
                    break;
                case 6:
                    if(checkLocationPermission(context)) {
                        context.startActivity(new Intent(context, WebView.class).putExtra("webview", "https://www.google.com/maps/place/Amrita+Vishwa+Vidyapeetham+University/@10.9016735,76.9012677,16z/data=!4m5!3m4!1s0x3ba85c95d3e828eb:0x2785cb4510629029!8m2!3d10.9026791!4d76.9006279"));
                    }else{
                        Toast.makeText(context, "You have not given location access!", Toast.LENGTH_SHORT).show();
                    }
                    break;
                default:
                    Toast.makeText(context, "Coming soon", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(context, "Please connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
        mViews.set(position, null);
    }

    private void bind(CardItem item, View view) {
        TextView titleTextView = (TextView) view.findViewById(R.id.titleTextView);

        ImageView imgview=(ImageView)view.findViewById(R.id.adapterimageview);
        titleTextView.setText(item.getTitle());

        imgview.setImageResource(item.getImage());
    }
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private boolean checkLocationPermission(final Context context) {
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(((Activity)context),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // context thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions((Activity)context,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions((Activity)context,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }
}
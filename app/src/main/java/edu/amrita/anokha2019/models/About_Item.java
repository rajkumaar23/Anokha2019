package edu.amrita.anokha2019.models;

public class About_Item {

    private int res_id;
    private String title;

    public About_Item(String title, int res_id){
        this.title=title;
        this.res_id=res_id;
    }

    public int getRes_id() {
        return res_id;
    }

    public String getTitle() {
        return title;
    }
}

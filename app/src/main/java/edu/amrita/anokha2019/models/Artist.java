package edu.amrita.anokha2019.models;

public class Artist {

    private String name,description, imageURL;
    private int musicFile;

    public Artist(String name, String description, String imageURL, int musicFile){
        this.name=name;
        this.description=description;
        this.imageURL = imageURL;
        this.musicFile =musicFile;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getMusicFile() {
        return musicFile;
    }

    public String getImageURL() {
        return imageURL;
    }
}

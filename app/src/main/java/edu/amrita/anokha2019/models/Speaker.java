package edu.amrita.anokha2019.models;

public class Speaker {

    private String name,designation,description,image_url;

    public Speaker(String name, String designation, String description, String image_url){
        this.name=name;
        this.designation=designation;
        this.description=description;
        this.image_url=image_url;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getDesignation() {
        return designation;
    }

    public String getImage_url() {
        return image_url;
    }
}

package edu.amrita.anokha2019.models;

public class Event {
    private String name,subtitle;
    private int id;
    private char letter;
    private String teamcode,paidby;


    public Event(String name, String subtitle, int id){
        this.name=name;
        this.subtitle=subtitle;
        this.id=id;
        try{
            letter=name.charAt(0);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    public Event() {

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getLetter() {
        return Character.toString(letter);
    }

    public void setTeamcode(String teamcode) {
        this.teamcode = teamcode;
    }

    public void setPaidby(String paidby) {
        this.paidby = paidby;
    }

    public String getTeamcode() {
        return teamcode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPaidby() {
        return paidby;
    }
}

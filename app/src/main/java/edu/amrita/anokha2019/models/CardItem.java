package edu.amrita.anokha2019.models;

public class CardItem {


    private int mTitleResource;
    private int mImageResource;

    public CardItem(int title,int img) {
        mTitleResource = title;

        mImageResource=img;
    }



    public int getTitle() {
        return mTitleResource;
    }
    public int getImage() {
        return mImageResource;
    }
}
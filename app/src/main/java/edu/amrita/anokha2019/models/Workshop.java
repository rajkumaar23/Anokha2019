package edu.amrita.anokha2019.models;

public class Workshop {
    private String name;
    private String teamcode;
    private String paidby;
    private String subtitle;
    private int id;
    private char letter;


    public Workshop(String name, String subtitle, int id){
        this.name=name;
        this.subtitle=subtitle;
        this.id=id;
        try{
            letter=name.charAt(0);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    public Workshop(String name,String teamcode,String paidby){

        this.name = name;
        this.teamcode = teamcode;
        this.paidby = paidby;
    }

    public Workshop() {

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getLetter() {
        return Character.toString(letter);
    }

    public String getPaidby() {
        return paidby;
    }

    public String getTeamcode() {
        return teamcode;
    }

    public void setPaidby(String paidby) {
        this.paidby = paidby;
    }

    public void setTeamcode(String teamcode) {
        this.teamcode = teamcode;
    }

    public void setName(String name) {
        this.name = name;
    }
}

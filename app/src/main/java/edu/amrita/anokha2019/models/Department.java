package edu.amrita.anokha2019.models;

public class Department {

    private String dept_name;
    private int dept_id;
    private char letter;

    public Department(String name,int id){
        this.dept_name=name;
        this.dept_id=id;
        try{
            letter=dept_name.charAt(0);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    public String getDept_name() {
        return dept_name;
    }

    public String getLetter() {
        return Character.toString(letter);
    }

    public int getDept_id() {
        return dept_id;
    }
}

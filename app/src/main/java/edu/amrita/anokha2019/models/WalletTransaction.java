package edu.amrita.anokha2019.models;

public class WalletTransaction {
    private String tid,details,vendor_name,amount,transacted_at;

    public WalletTransaction(String tid, String details, String vendor_name, String amount, String transacted_at) {
        this.tid = tid;
        this.details = details;
        this.vendor_name = vendor_name;
        this.amount = amount;
        this.transacted_at = transacted_at;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getVendor_name() {
        return vendor_name;
    }

    public void setVendor_name(String vendor_name) {
        this.vendor_name = vendor_name;
    }

    public String getTransacted_at() {
        return transacted_at;
    }

    public void setTransacted_at(String transacted_at) {
        this.transacted_at = transacted_at;
    }
}

package edu.amrita.anokha2019.helpers;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.v4.app.ActivityCompat;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import edu.amrita.anokha2019.R;

@TargetApi(Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    // You should use the CancellationSignal method whenever your app can no longer process user input, for example when your app goes
    // into the background. If you don’t use this method, then other apps will be unable to access the touch sensor, including the lockscreen!//

    private CancellationSignal mCancellationSignal;
    private Context context;
    private String username,password,url_login;
    private CheckBox box;
    private AlertDialog dialog;

    private TextView status;
    private ImageView icon;

    public FingerprintHandler(Context mContext, String username,String password, CheckBox remember_me, String url_login,AlertDialog dialog) {
        context = mContext;
        this.username=username;
        this.password=password;
        this.box=remember_me;
        this.url_login=url_login;
        this.dialog=dialog;
        status=dialog.findViewById(R.id.fingerprint_status);
        icon=dialog.findViewById(R.id.fingerprint_icon);

    }

    public void stopListening() {
        if (mCancellationSignal != null) {
            mCancellationSignal.cancel();
            mCancellationSignal = null;
        }
    }

    //Implement the startAuth method, which is responsible for starting the fingerprint authentication process//

    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {

        mCancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, mCancellationSignal, 0, this, null);
    }

    @Override
    //onAuthenticationError is called when a fatal error has occurred. It provides the error code and error message as its parameters//

    public void onAuthenticationError(int errMsgId, CharSequence errString) {

        //I’mediaPlayer going to display the results of fingerprint authentication as a series of toasts.
        //Here, I’mediaPlayer creating the message that’ll be displayed if an error occurs//

        status.setText("Authentication error\n" + errString);
    }

    @Override

    //onAuthenticationFailed is called when the fingerprint doesn’t match with any of the fingerprints registered on the device//

    public void onAuthenticationFailed() {
        status.setText("Authentication failed");
        icon.setImageResource(R.drawable.ic_fingerprint_error);
    }

    @Override

    //onAuthenticationHelp is called when a non-fatal error has occurred. This method provides additional information about the error,
    //so to provide the user with as much feedback as possible I’mediaPlayer incorporating this information into my toast//
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        status.setText("Authentication help\n" + helpString);
    }@Override

    //onAuthenticationSucceeded is called when a fingerprint has been successfully matched to one of the fingerprints stored on the user’s device//
    public void onAuthenticationSucceeded(
            FingerprintManager.AuthenticationResult result) {
        status.setText("Success");
        icon.setImageResource(R.drawable.ic_fingerprint_success);
        if(checkNetwork.isConnected(context))
        new okLogin(context,username,password,box,url_login).execute();
        dialog.dismiss();
    }

}
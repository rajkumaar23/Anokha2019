package edu.amrita.anokha2019.helpers;

import android.content.Context;

import edu.amrita.anokha2019.R;
import okhttp3.CertificatePinner;

public class SSLPin {


    public static CertificatePinner getCertPinner(Context context){
        String hostname="anokha.amrita.edu";
        String pin=context.getResources().getString(R.string.ssl_pin);
        return new CertificatePinner.Builder()
                .add(hostname,pin)
                .build();
    }
}

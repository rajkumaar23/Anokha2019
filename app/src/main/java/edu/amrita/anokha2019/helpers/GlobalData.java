package edu.amrita.anokha2019.helpers;

import android.content.Context;
import android.media.MediaPlayer;

import org.json.JSONObject;

import java.util.ArrayList;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.models.Artist;
import edu.amrita.anokha2019.models.Speaker;

public class GlobalData {

    public static String username;
    public static String password;
    public static boolean signedin = false;
    public static int id;
    public static String token;
    public static String name;
    public static String anokhaID;
    public static String email;
    public static String mobile;
    public static String college;
    public static String lumiereID;
    public static JSONObject speakers;
    public static ArrayList<Speaker> speakersList;

    public static String version;

    public static boolean checkSignIn(){
        return signedin;
    }

    public static JSONObject artists;
    public static ArrayList<Artist> artistsList = new ArrayList<>();
    public static MediaPlayer mediaPlayer;

    /*public static void initArtists(Context context){
        artists.clear();
        artists.add(new Artist("Naresh Iyer",context.getString(R.string.lorem),R.drawable.one,R.raw.naresh));
        artists.add(new Artist("Saindhavi",context.getString(R.string.lorem),R.drawable.three,R.raw.sunitha));
    }*/
}

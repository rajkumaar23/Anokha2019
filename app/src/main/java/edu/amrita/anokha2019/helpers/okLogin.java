package edu.amrita.anokha2019.helpers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.json.JSONObject;

import edu.amrita.anokha2019.R;
import edu.amrita.anokha2019.activities.ChangePasswordActivity;
import edu.amrita.anokha2019.activities.MainActivity;
import okhttp3.CertificatePinner;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class okLogin extends AsyncTask<Void,Void,Void> {

    private String firebase_token,firebase_token_url;
    private boolean firebase_refresh;


    @SuppressLint("StaticFieldLeak")
    private Context mContext;
    private String username,password,res,url_login;
    @SuppressLint("StaticFieldLeak")
    private
    CheckBox box;

    private boolean password_reset = false;
    
    public okLogin(Context context, String username, String password, CheckBox remember_me, String url_login){
        mContext=context;
        this.username=username;
        this.password=password;
        this.box=remember_me;
        this.url_login=url_login;
    }


    private ProgressDialog dialog;

    @Override
    protected void onPostExecute(Void aVoid) {
        if(!password_reset) {
            try {
                JSONObject jsonObject = new JSONObject(res);
                if (!jsonObject.getBoolean("success")) {
                    Toast.makeText(mContext, jsonObject.getString("error"), Toast.LENGTH_LONG).show();
                } else if (jsonObject.getBoolean("success")) {
                    // Toast.makeText(mContext,"Successfully signed in",Toast.LENGTH_SHORT).show();
                    JSONObject data = jsonObject.getJSONObject("data");
                    GlobalData.signedin = true;
                    GlobalData.id = data.getInt("id");
                    GlobalData.name = data.getString("name");
                    GlobalData.anokhaID = data.getString("anokha_id");
                    GlobalData.email = data.getString("email");
                    GlobalData.mobile = data.getString("phone_number");
                    GlobalData.college = data.getString("college");
                    if(data.has("lumiereID")) {
                        GlobalData.lumiereID = data.getString("lumiereID");
                    }


                    mContext.startActivity(new Intent(mContext, MainActivity.class));
                    ((Activity)mContext).finish();
//                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                } else {
                    Toast.makeText(mContext, "Error", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Toast.makeText(mContext, "Error connecting to server", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }else{
            mContext.startActivity(new Intent(mContext,ChangePasswordActivity.class).putExtra("password_reset",true));
        }
        try {
            if (dialog.isShowing())
                dialog.dismiss();
        }catch (Exception e){
            e.printStackTrace();
        }

        super.onPostExecute(aVoid);
    }

    @Override
    protected void onPreExecute() {

        firebase_token_url=mContext.getResources().getString(R.string.url_firebase_token);

        SharedPreferences firebase = mContext.getSharedPreferences("firebase",Context.MODE_PRIVATE);
        firebase_token=firebase.getString("token",null);

        SharedPreferences pref = mContext.getSharedPreferences("user",Context.MODE_PRIVATE) ;
        SharedPreferences.Editor ed = pref.edit();
        if (box.isChecked()){
            ed.putString("username",username);
            ed.putString("password",password);
            ed.apply();
        }else{
            ed.putString("username",null);
            ed.putString("password",null);
            ed.apply();
        }

        try {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Logging in");
            dialog.setCancelable(false);
            dialog.show();
        }catch (Exception e){
            e.printStackTrace();
            Crashlytics.log(e.getMessage());
        }
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try{
            CertificatePinner certificatePinner = SSLPin.getCertPinner(mContext);
            OkHttpClient client = new OkHttpClient.Builder()
                    .certificatePinner(certificatePinner)
                    .build();

            RequestBody body = new FormBody.Builder()
                    .add("email",username)
                    .add("password",password)
                    .build();

            Request request = new Request.Builder()
                    .url(url_login)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            assert response.body() != null;
            res= response.body().string();
            String jwt=new JSONObject(res).getJSONObject("data").getString("token");
            GlobalData.token=jwt;
            Log.e("JWT TOKEN - ",jwt);

            request=new Request.Builder()
                    .url(firebase_token_url)
                    .addHeader("Authorization","Bearer "+GlobalData.token)
                    .get()
                    .build();

            response = client.newCall(request).execute();
            assert response.body() != null;
            if(!new JSONObject(response.body().string()).getString("message").trim().equals(firebase_token.trim())){
                body = new FormBody.Builder()
                        .add("id",firebase_token)
                        .build();

                request=new Request.Builder()
                        .url(firebase_token_url)
                        .addHeader("Authorization","Bearer "+jwt)
                        .post(body)
                        .build();
                response =client.newCall(request).execute();
                JSONObject obj=new JSONObject(response.body().string());
                if(obj.getBoolean("success")){
                    Log.e("FIREBASE TOKEN - ",obj.getString("message"));
                }
            }
            GlobalData.username = username;
            GlobalData.password = password;

            //Checking if password reset

            if(new JSONObject(res).getJSONObject("data").getBoolean("has_password_reset")){
                password_reset=true;
                return null;
            }

            request = new Request.Builder()
                    .url("https://anokha.amrita.edu/api/me")
                    .addHeader("Authorization","Bearer "+jwt)
                    .get()
                    .build();
            response =client.newCall(request).execute();
            res= response.body().string();
            Log.e("JSON ",res);

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
